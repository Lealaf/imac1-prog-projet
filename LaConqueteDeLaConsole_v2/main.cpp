/*   REMARQUE POUR CHANGER DE MODE
Console en SDL:
*   decomenter toute la partie MODE SDL ( Ctrl+shift+X)
*   decomenter return modeSDL();
*   comenter return modeConsole();
*
SDL en Console:
*   comenter toute la partie MODE SDL ( Ctrl+shift+C)
*   comenter return modeSDL();
*   decomenter return modeConsole();
*/

//------------- MODE SDL--------------//
//#include <windows.h>

#ifdef __cplusplus
    #include <cstdlib>
#else
    #include <stdlib.h>
#endif

// Bibliotheque_SDL_GL
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glu.h>

// Bibliotheque std
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// Include autre fichier
#include <Plateau.h>
#include <affichagePlateau.h>
#include <sdlMove.h>
#include <PartieSdl.h>


int main(int argc, char** argv)
{
    PartieSDL(0);
}


//------------- FIN MODE SDL--------------//
//
//#include <iostream>
//#include "PartieConsole.h"
//#include "fonctionMenuConsole.h"
//
//using namespace std;
//const int MAXJOUEURMain = 10;
//
//int modeConsole();
//
//int main ( int argc, char** argv )
//{
//    //Mecanique de jeu en console
//    modeConsole();
//    return 0;
//
//    // modeSDL();
//}
//
//
//int modeConsole()
//{
//	while(menuJouerQuitter())
//    {
//        string nomListeTxt;
//        int num;
//        if (menuNewChargerPartieQuitter(nomListeTxt, num))
//        {
//            PartieConsole* part1 = new PartieConsole(nomListeTxt,num);
//            part1->lancerPartie();
//            part1->finPartieConsole();
//            delete part1;
//        }
//    }
//
//}
//
//
//
