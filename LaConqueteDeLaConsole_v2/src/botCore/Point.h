#ifndef POINT_H
#define POINT_H


struct Point{
    int pX; //pas touche
    int pY;//pas touche
    Point* suivant;//pas touche

    float cost = 0;
    float heuristique = 0;
    Point* parent = NULL;

    bool operator == (const Point& other) const {
        if (pX == other.pX && pY == other.pY) return true;
        return false;
    }

};

#endif // POINT_H
