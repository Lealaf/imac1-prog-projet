#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <stdlib.h>
//#include <iostream>

#include "Point.h"


class Noeud
{
    private:
        Point* pointN;
        Noeud* droite;
        Noeud* gauche;


    public:
        Noeud();
        Noeud(Point* newPoint);
        virtual ~Noeud();

        void insertPoint(Point* pointaInsert);
        Point* insertPointRetoueAdresse(Point* pointaInsert);
        bool estDansArbre (Point* point);



};

#endif // BINARYTREE_H
