/*Note pour @Judicaelle:
* pour utiliser botCore
*
*teste si le joueur est un bot (joueur.getIsBoot())
*   BotCore* bot = new BotCore(partie->getPlateau(), idDuJoueurBot);
*   bot->jouer();
*   delete bot;
*/

#ifndef BOTCORE_H
#define BOTCORE_H

#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <algorithm>

#include <sdlMove.h>
#include <affichageJeu.h>
#include "Plateau.h"
#include "binarytree.h"
#include "Point.h"


const int MAXCHEMIN = MAXGRILLE*4;

/*@Gregoire: tu va en avoir besoin pour aStar
*tu peux modifier et rajouter des variable sauf les pas touche
*/

struct Liste{
    Point* premier;
    int nbProint;
};

class BotCore
{
    private:
        int idBot;
        Plateau* plateau;
        int cibleX;
        int cibleY;

        Liste listeUniteBot;
        Liste listeUniteEnnemie;

        bool cibleExiste();
        //choisi une cible
        void choixCible(int pX, int pY);
        //Selectionne une cible possible aléatoirement pour que tout le troupeau aille dessus, si la cible meurt on rappelle cette fonction
        void getRandomCible();

        //unite bot attaque cible
        void attaqueUniterBot(int pXAttaquante, int pYAttaquante);
        void essaisAttaqueUniterAPorter(int pXAttaquante, int pYAttaquante);

        void deplacerUniterBot(int& pX, int& pY);

        //permet de teste le deplacement possible d'Unite si elle ce trouvais en (departX, departY) pour aller en (newX, newY)
        bool deplacementSuposerPossible(int pXUnite, int pYUnite,int departX, int departY, int newX, int newY);

        //permet d'enlever la première uniter d'une liste
        void enleverUniterListe(Liste& liste);

        void enleverCibleListe(Liste& liste);

        //Utilise l'algorithme A-Star pour trouver le plus court chemin entre deux points du plateau
        int aStar(int departX, int departY, Point chemin[MAXCHEMIN]);

        std::vector<Point> getVoisins(Point point, bool moveDiagonalyEnabled);

        bool isInList(std::vector<Point> &liste, Point target);
        bool isInVisited(std::vector<Point*> &liste, Point target);

        bool isInListWithMoindreCout(std::vector<Point> &liste, Point target);

        Point getLowestCostPoint(std::vector<Point> points);

        float getEuclidienneDistance(int fromX, int fromY, int toX, int toY );

        int construireChemin(Point cheminGenere[MAXCHEMIN], Point current );

        void openListInser(std::vector<Point> & openList, Point aInsert );

        Point* depile (std::vector<Point> & points);


    public:
        BotCore(Plateau* plateauCourant, int idBotCourant);
        virtual ~BotCore();
        //Lance le jeu du bot
        void jouer(Partie* partie);
};

#endif // BOTCORE_H
