#include "binarytree.h"

using namespace std;

Noeud::Noeud()
{
    pointN=NULL;
    droite=NULL;
    gauche=NULL;
}

Noeud::Noeud(Point* newPoint)
{
    pointN=newPoint;
    droite=NULL;
    gauche=NULL;
}

Noeud::~Noeud()
{
    delete pointN;
    delete droite;
    delete gauche;
}


void Noeud::insertPoint(Point* pointaInsert)
{
    if (pointN==NULL)
    {
        new Noeud(pointaInsert);
    }else if(pointN->heuristique < pointaInsert->heuristique)
    {
        if(droite == NULL)
        {
            droite = new Noeud(pointaInsert);
        }else
        {
            droite->insertPoint(pointaInsert);
        }
    }else
    {
        if(gauche == NULL)
        {
            gauche = new Noeud(pointaInsert);
        }else
        {
            gauche->insertPoint(pointaInsert);
        }
    }
}

Point* Noeud::insertPointRetoueAdresse(Point* pointaInsert)
{
    if (pointN==NULL)
    {
        pointN =pointaInsert;
        return pointN;
    }else if(pointN->heuristique < pointaInsert->heuristique)
    {
        if(droite == NULL)
        {
            droite = new Noeud(pointaInsert);
            return droite->pointN;
        }else
        {
            return droite->insertPointRetoueAdresse(pointaInsert);
        }
    }else
    {
        if(gauche == NULL)
        {
            gauche = new Noeud(pointaInsert);
            return gauche->pointN;
        }else
        {
            return gauche->insertPointRetoueAdresse(pointaInsert);
        }
    }
}

bool Noeud::estDansArbre(Point* point)
{
    if (pointN == NULL)
    {
        return false;
    }else
        {
            if(pointN->pX == point->pX && pointN->pY == point->pY)
        {
            return true;
        }
        if(pointN->heuristique < point->heuristique && droite != NULL){
            return droite->estDansArbre(point);
        }
        if(pointN->heuristique >= point->heuristique && gauche != NULL){
            return gauche->estDansArbre(point);
        }
        return false;
    }
    return false;
}

