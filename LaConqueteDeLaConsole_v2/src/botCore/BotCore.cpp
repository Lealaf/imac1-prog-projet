#include "BotCore.h"

using namespace std;

BotCore::BotCore(Plateau* plateauCourant, int idBotCourant)
{
    plateau = plateauCourant;
    idBot=idBotCourant;


    cibleX= -1;
    cibleY= -1;
    //impl�mentation des listes
    listeUniteBot.nbProint =0;
    listeUniteBot.premier=NULL;

    listeUniteEnnemie.nbProint =0;
    listeUniteEnnemie.premier=NULL;

    for (int i=0; i<plateau->getTaille(); i++)
    {
        for (int j=0; j<plateau->getTaille(); j++)
        {
            if (plateau->uniteExiste(i,j))
            {
                Point* ptps= new Point;
                ptps->pX= i;
                ptps->pY= j;
                if(plateau->uniteExiste(i,j))
                {
                    if( plateau->getIdJoueurUnite(i,j)== idBot)
                    {
                        ptps->suivant= listeUniteBot.premier;
                        listeUniteBot.premier=ptps;
                        listeUniteBot.nbProint ++;
                    }
                    else
                    {
                        ptps->suivant= listeUniteEnnemie.premier;
                        listeUniteEnnemie.premier=ptps;
                        listeUniteEnnemie.nbProint ++;
                    }
                }

            }
        }
    }
//    cout<<"1: listeUniteEnnemie.nbProint :"<<listeUniteEnnemie.nbProint<<endl;
//    Point* ptr= listeUniteEnnemie.premier;
//	while(ptr!=NULL)
//    {
//        cout<<"   "<<ptr->pX<<","<<ptr->pY<<endl;
//        ptr=ptr->suivant;
//    }
//    cout<<"1: listeUniteBot.nbProint :"<<listeUniteBot.nbProint<<endl;
//    ptr= listeUniteBot.premier;
//	while(ptr!=NULL)
//    {
//        cout<<"   "<<ptr->pX<<","<<ptr->pY<<endl;
//        ptr=ptr->suivant;
//    }
}


BotCore::~BotCore()
{
    plateau= NULL;

    cibleX= -1;
    cibleY= -1;
    while(listeUniteBot.premier != NULL)
    {
        assert(listeUniteBot.nbProint>0);
        enleverUniterListe(listeUniteBot);
    }
    while(listeUniteEnnemie.premier != NULL)
    {
        assert(listeUniteEnnemie.nbProint>0);
        enleverUniterListe(listeUniteEnnemie);
    }
}

void BotCore::jouer(Partie* partie)
{
    while(listeUniteBot.nbProint>0&& listeUniteEnnemie.nbProint >0)
    {
        int unitePX=listeUniteBot.premier->pX;
        int unitePY=listeUniteBot.premier->pY;

        if(!cibleExiste())
        {
            choixCible(unitePX,unitePY);
        }

        SDL_Delay(1000);
        afficheOnglet(idBot, plateau,true, partie,unitePX,unitePY);
        affichagePlateau(plateau);
        affichageAccesAttaque(unitePX , unitePY, plateau, idBot);
        affichageAccesDeplacement(unitePX , unitePY, plateau, idBot);
        refreshSDL();
        SDL_Delay(1500);

        if(plateau->attaquerPossible(unitePX,unitePY, cibleX, cibleY))
        {
            // Affichage de l'attaque
            affichageAttaque(plateau, unitePX, unitePY, cibleX, cibleY,true);

            attaqueUniterBot(unitePX,unitePY);
            enleverUniterListe(listeUniteBot);

            // Affichage du Plateau
            afficheOnglet(idBot, plateau,false, partie,unitePX,unitePY);
            affichagePlateau(plateau);
            refreshSDL();
            SDL_Delay(1000);
        }else{
            deplacerUniterBot(unitePX,unitePY);

            // Affichage du Plateau
            afficheOnglet(idBot, plateau,false, partie,unitePX,unitePY);
            affichagePlateau(plateau);
            refreshSDL();
            SDL_Delay(1000);

            if(plateau->attaquerPossible(unitePX,unitePY, cibleX, cibleY))
            {
                // Affichage de l'attaque
                affichageAttaque(plateau, unitePX, unitePY, cibleX, cibleY, true);
                attaqueUniterBot(unitePX,unitePY);

                // Affichage du Plateau
                afficheOnglet(idBot, plateau,false, partie,unitePX,unitePY);
                affichagePlateau(plateau);
                refreshSDL();
            }else{
                essaisAttaqueUniterAPorter(unitePX,unitePY);
            }
            enleverUniterListe(listeUniteBot);
        }
    }
}

bool BotCore::cibleExiste()
{
    if(cibleX==-1 || cibleY==-1)
    {
        return false;
    }
    return true;
}

void BotCore::choixCible(int pX, int pY)
{
    cibleX= -1;
    cibleY= -1;
    if(plateau->uniteAporte(pX,pY))
    {
        int porte = plateau->getPorteUnite(pX,pY);
        int i=pX-porte;
        int j=pY-porte;
        while(cibleX== -1 && cibleY== -1 && i<=pX+porte)
        {
            while(cibleX== -1 && cibleY== -1 && j<=pY+porte)
            {
                if (plateau->uniteExiste(i,j)&& plateau->getIdJoueurUnite(pX,pY)!=plateau->getIdJoueurUnite(i,j))
                {
                    cibleX=i;
                    cibleY=j;
                }
                j++;
            }
            j=pY-porte;
            i++;
        }
    }
    else
    {
        getRandomCible();
    }
}

void BotCore::getRandomCible()
{
    srand(time(NULL));
	int nbAleat = rand()%listeUniteEnnemie.nbProint;
	Point* ptr= listeUniteEnnemie.premier;
	int i=0;
	while(i<nbAleat && ptr!=NULL && ptr->suivant!=NULL)
    {
        ptr=ptr->suivant;
        i++;
    }
    cibleX=ptr->pX;
    cibleY=ptr->pY;
}

void BotCore::attaqueUniterBot(int pXAttaquante, int pYAttaquante)
{
    plateau->attaquerUnite(pXAttaquante,pYAttaquante,cibleX,cibleY);
    if(plateau->getPtVieUnite(cibleX,cibleY)<=0)
    {
        enleverCibleListe(listeUniteEnnemie);
        cibleX=-1;
        cibleY=-1;
        plateau->enleverUniteMorte();

    }
}

void BotCore::essaisAttaqueUniterAPorter(int pXAttaquante, int pYAttaquante)
{
    assert(plateau->uniteExiste(pXAttaquante,pYAttaquante));
    if(plateau->uniteAporte(pXAttaquante,pYAttaquante))
    {
        int porte = plateau->getPorteUnite(pXAttaquante,pYAttaquante);
        int i=pXAttaquante-porte;
        int j=pYAttaquante-porte;
        bool attaqueReussi=false;
        while(i<=pXAttaquante+porte && !attaqueReussi)
        {
            while(j<=pYAttaquante+porte && !attaqueReussi)
            {
                if (plateau->uniteExiste(i,j)&& plateau->getIdJoueurUnite(pXAttaquante,pYAttaquante)!=plateau->getIdJoueurUnite(i,j))
                {
                    affichageAttaque(plateau, pXAttaquante, pYAttaquante, i, j, true);
                    attaqueReussi=plateau->attaquerUnite(pXAttaquante, pYAttaquante,i, j);
                    if(attaqueReussi &&plateau->getPtVieUnite(i, j)<=0)
                    {
                        enleverCibleListe(listeUniteEnnemie);
                        plateau->enleverUniteMorte();

                    }else if (attaqueReussi && plateau->getPtVieUnite(i, j) <plateau->getPtVieUnite(cibleX,cibleY))
                    {
                        cibleX =i;
                        cibleY =j;
                    }

                }
                j++;
            }
            j=pYAttaquante-porte;
            i++;
        }
    }
}

void BotCore::deplacerUniterBot(int& pX, int& pY)
{

    Point chemin[MAXCHEMIN];
    int tailleTabChemin = aStar(pX,pY,chemin);
    int tailleDeplacement= plateau->getMobiliteUnite(pX,pY);
    if (tailleTabChemin-1<tailleDeplacement)
    {
        tailleDeplacement=tailleTabChemin-1;
    }
    if(plateau->deplacerUnite(pX,pY,chemin[tailleDeplacement].pX,chemin[tailleDeplacement].pY))
    {
        pX=chemin[tailleDeplacement].pX;
        pY=chemin[tailleDeplacement].pY;
    }

}

void BotCore::enleverUniterListe(Liste& liste)
{
    assert(liste.premier!=NULL);
    Point* ptps= liste.premier;
    liste.premier= ptps->suivant;
    delete ptps;
    ptps=NULL;
    liste.nbProint--;
}

void BotCore::enleverCibleListe(Liste& liste)
{
//    Point* ptr1= listeUniteEnnemie.premier;

//    cout<<"    avant :"<<listeUniteEnnemie.nbProint<<endl;
//    ptr1= listeUniteEnnemie.premier;
//    while(ptr1!=NULL)
//    {
//        cout<<"       "<<ptr1->pX<<","<<ptr1->pY<<endl;
//        ptr1=ptr1->suivant;
//    }

    assert(liste.nbProint!=0);
    assert(liste.premier!=NULL);
    if(liste.premier->pX==cibleX && liste.premier->pY==cibleY)
    {
        enleverUniterListe(liste);
    }else
    {
        Point* ptr= liste.premier;
        Point* precedent= liste.premier;

        while(ptr->pX!=cibleX || ptr->pY!=cibleY)
        {
            assert(ptr!=NULL);
            precedent=ptr;
            ptr=ptr->suivant;
        }

        precedent->suivant= ptr->suivant;
        delete ptr;
        ptr=NULL;
        liste.nbProint--;
//        ptr1= listeUniteEnnemie.premier;

//        cout<<"    apres :"<<listeUniteEnnemie.nbProint<<endl;
//        ptr1= listeUniteEnnemie.premier;
//        int i=0;
//        cout<<"     ptr1->"<<ptr1<<endl;
//        while(ptr1!=NULL)
//        {
//            cout<<"       "<<i<<": "<<ptr1->pX<<","<<ptr1->pY<<endl;
//            ptr1=ptr1->suivant;
//            i++;
//        }
    }
}

bool BotCore::deplacementSuposerPossible(int pXUnite, int pYUnite, int departX, int departY, int newX, int newY)
{
    if( (plateau->coordonnerExiste(pXUnite, pYUnite)&&plateau->coordonnerExiste(departX, departY)&& plateau->coordonnerExiste(newX, newY)) //coordonner ds le plateau
            && plateau->uniteExiste(pXUnite,pYUnite) // l'uniter existe
            && (abs(departX-newX )<= plateau->getMobiliteUnite(pXUnite, pYUnite))
            && (abs(departY-newY )<= plateau->getMobiliteUnite(pXUnite, pYUnite))
            && !plateau->estUnObstacle(newX,newY)
            && !plateau->uniteExiste(newX,newY)
      )
    {
        return true;
    }
    else
    {
        return false;
    }
}


int BotCore::aStar(int departX, int departY, Point chemin[MAXCHEMIN])
{

    //Cellules de la grille d�j� visit�es par A*
    Noeud arbreVisited;
    //cellules les plus int�ressantes
    vector <Point> openList;

    Point start, current, destination;

    destination.pX = this->cibleX;
    destination.pY = this->cibleY;

    start.pX = departX;
    start.pY = departY;
    start.heuristique = getEuclidienneDistance(start.pX, start.pY, destination.pX, destination.pY);

    openList.push_back(start);
    while( openList.size() != 0 )
    {
        /*cout<<"openList"<<endl;
        for(unsigned int iPatate=0; iPatate < openList.size(); iPatate++)
        {
            cout<<"  "<<openList[iPatate].pX<<","<<openList[iPatate].pY<<" : "<<openList[iPatate].cost<<"->"<< openList[iPatate].heuristique;
            if(openList.size()>1)
            {
                cout<<" p:"<<openList[iPatate].parent->pX<<","<<openList[iPatate].parent->pY<<endl;
            }else
            {
                cout<<endl;
            }
        }*/

        //on extrait le point le plus proche de la cible
        current = *depile(openList);
        //cout<<"depile->  "<<current.pX<<","<<current.pY<<" : "<< current.heuristique<<endl;

        Point* pTps = new Point;
        *pTps=current;
        Point* currentPtr = arbreVisited.insertPointRetoueAdresse(pTps);


        if( current == destination)
        {
            return construireChemin(chemin, current);
        }

        // On cherche les cases autour du point
        vector<Point> voisins = getVoisins(current, true);
        //on parcours les voisins
        for(unsigned int i=0; i < voisins.size(); i++)
        {
            voisins[i].cost = current.cost + 1;
            voisins[i].heuristique = getEuclidienneDistance(voisins[i].pX, voisins[i].pY, destination.pX, destination.pY);
            //cout<<"  v: "<<voisins[i].pX<<","<<voisins[i].pY<<"  "<<voisins[i].cost<<"->"<< voisins[i].heuristique<<endl;
            //Si le voisin a de l'interet pour a-star on l'utilise
            if( !arbreVisited.estDansArbre(&voisins[i]) &&  !isInListWithMoindreCout(openList, voisins[i]) )
            {

                //on lui passe dans sa propriete "parent" le pointeur de son parent visit�
                voisins[i].parent = currentPtr;
                openListInser(openList,voisins[i]);
                Point* pTps = new Point;
                *pTps=voisins[i];
                arbreVisited.insertPoint(pTps);

            }
        }
    }

    return 0;
}

vector<Point> BotCore::getVoisins(Point point, bool moveDiagonalyEnabled)
{
    vector<Point> voisins;
    int x = 0;
    int y = 0;

    for(int i = point.pX-1; i <= point.pX+1; i++  )
    {
        for(int j = point.pY-1; j<= point.pY+1; j++  )
        {
            x = i;
            y = j;

            if( this->plateau->coordonnerExiste(x,y) && !this->plateau->estUnObstacle(x,y)
               && (x!= point.pX || y!=point.pY))
            {
                if (!this->plateau->uniteExiste(x,y) || (this->plateau->uniteExiste(x,y) && x== this->cibleX &&y ==this->cibleY))
                {
                    Point v;
                    v.pX = x;
                    v.pY = y;
                    voisins.push_back(v);
                }
            }
        }
    }
    return voisins;
}

Point BotCore::getLowestCostPoint(vector<Point> points)
{
    Point minimum = points[0];

    for (unsigned int i = 1; i < points.size(); i++)
    {
        if (points[i].heuristique < minimum.heuristique)
            minimum = points[i];
    }
    return minimum;
}

bool BotCore::isInList(vector<Point> &liste, Point target)
{
    for(unsigned int i = 0; i < liste.size(); i++ )
    {
        if( liste[i] == target)
            return true;
    }
    return false;
}

bool BotCore::isInVisited(std::vector<Point*> &liste, Point target)
{
    for(unsigned int i = 0; i < liste.size(); i++ )
    {
        if( liste[i]->pX == target.pX && liste[i]->pY==target.pY)
        {
            return true;
        }
    }
    return false;
}

bool BotCore::isInListWithMoindreCout(vector<Point> &liste, Point target)
{
    for( unsigned int i=0; i < liste.size(); i++ )
    {
        if( liste[i] == target && liste[i].heuristique < target.heuristique )
            return true;
    }
    return false;
}

float BotCore::getEuclidienneDistance(int fromX, int fromY, int toX, int toY )
{
    return sqrt(pow(toX-fromX,2)+pow(toY-fromY,2));
}

int BotCore::construireChemin(Point cheminGenere[MAXCHEMIN], Point current )
{
    vector<Point> chemin;
    Point* temp = current.parent;

    //on extrait le chemin dans le sens inverse && on le stocke
    while( temp != NULL )
    {
        //cout<<temp->pX<<","<<temp->pY<<endl;
        chemin.push_back(*temp);
        temp = temp->parent;

    }


    //on retoune le tableau pour avoir de chemin du d�part � l'arriv�e
    reverse(chemin.begin(), chemin.end());
    /*for( unsigned int i=0; i < chemin.size()-1; i++ )
     {
        cout<<chemin[i].pX<<","<<chemin[i].pY<<endl;
     }*/

     for( unsigned int i=0; i < chemin.size(); i++ )
     {

        cheminGenere[i] = chemin[i];
     }
    return chemin.size();
}

void BotCore::openListInser(std::vector<Point>& openList, Point aInsert)
{
    int i=0;
    while (i< openList.size() && aInsert.heuristique > openList[i].heuristique)
    {
        i++;
    }
    openList.insert(openList.begin()+i,aInsert);
}

Point* BotCore::depile(std::vector<Point> & points)
{
    assert(points.size()>0);
    Point* premierPoint= new Point;
    *premierPoint= points[0];
    points.erase(points.begin());
    //cout<<"erase->"<<premierPoint->pX<<","<<premierPoint->pY<<endl;
    return premierPoint;
}
