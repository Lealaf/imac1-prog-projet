#include "Partie.h"

using namespace std;


Partie::Partie(std::string nomListeTxt, int num)
{
    assert(nomListeTxt=="listeFichier.txt" || nomListeTxt=="listePartieEnregistree.txt");

    string nomFichier;

    if (nomListeTxt== "listeFichier.txt")
    {
        string nomF = nomNumeroFichierTxt("listeFichier.txt", num);
        nomFichier= "./data/plateau/"+ nomF;
    }
    if (nomListeTxt=="listePartieEnregistree.txt")
    {
        string nomF = nomNumeroFichierTxt("listePartieEnregistree.txt", num);
        nomFichier= "./data/sauv/"+ nomF;
    }

    string const nomFichierTxtConst=nomFichier;
    ifstream fichier(nomFichierTxtConst.c_str());  //Ouverture d'un fichier en lecture
    assert (fichier.is_open());

    nbJoueur=0;
    string mot;
    string txtArgent;
    fichier >> idJoueurTour;
    while(fichier >> mot && mot != "plateau:") //Tant qu'on n'est pas � la fin, on lit
    {
        fichier >> txtArgent;
        tabJoueur[nbJoueur]=new Joueur(mot, stringToInt(txtArgent));
        nbJoueur++;
    }
    fichier.close();
    tabUnitePrix [0]=20;
    tabUnitePrix [1]=30;
    tabUnitePrix [2]=80;
    tabUnitePrix [3]=80;
    plateauPartie = new Plateau(nomFichierTxtConst);

    continuerPartie=true;
}

Partie::~Partie()
{
    for (int i=0; i<nbJoueur; i++)
    {
        delete tabJoueur[i];
    }
    nbJoueur=0;
    delete plateauPartie;
}

int Partie::getIdJoueurTour()
{
    return idJoueurTour;
}

Plateau* Partie::getPlateau()
{
    assert(plateauPartie!=NULL);
    return plateauPartie;
}

Joueur* Partie::getJoueur(int idJ)
{
    assert(tabJoueur[idJ]!=NULL);
    return tabJoueur[idJ];
}

bool Partie::getContinuerPartie()
{
    return continuerPartie;
}

void Partie::setContinuerPartie(bool newBool)
{
    continuerPartie=newBool;
}

bool Partie::joueurAPerdu(int idJoueur)
{
    if (plateauPartie->nbUniteJoueur(idJoueur)== 0)
    {
        return true;
    }else
    {
        return false;
    }
}

bool Partie::achatUnitePossible(int pX, int pY,int idUnite)
{
    assert(idUnite<NBUNITE);
    if (!plateauPartie->uniteExiste(pX,pY)&& tabJoueur[idJoueurTour]->getArgent()>=tabUnitePrix[idUnite])
    {
        return true;
    }else
    {
        return false;
    }
}

bool Partie::acheterUnite(int pX, int pY,int idUnite)
{
    if(achatUnitePossible(pX, pY, idUnite))
    {
        tabJoueur[idJoueurTour]->enleverArgent(tabUnitePrix[idUnite]);
        plateauPartie->ajoutUnite(pX,pY,idJoueurTour,idUnite);
        return true;
    }else
    {
        return false;
    }
}

bool Partie::peutJouerUnite(int IdJ)
{
    for (int i=0; i<plateauPartie->getTaille(); i++)
    {
        for (int j=0; j<plateauPartie->getTaille(); j++)
        {
            if (plateauPartie->uniteExiste(i,j) && (plateauPartie->getIdJoueurUnite(i,j)== IdJ) && (!plateauPartie->getADeplaceUnite(i,j) || !plateauPartie->getAAttaqueUnite(i,j)))
                {
                    return true;
                }
        }
    }
    return false;
}

int Partie::joueurGagnant()
{
    int JGagnant=-1;
    for (int i=0; i<nbJoueur; i++)
    {
        if (!joueurAPerdu(i)) //joueur n'a pas perdu
        {
            if (JGagnant!=-1) // joueur n'est pas le premier a ne pas perdre
            {
                return -1;
            }else
            {
                JGagnant = i;
            }
        }
    }
    return JGagnant;
}

void Partie::impotJoueur()
{
    for (int i=0; i<plateauPartie->getTaille(); i++)
    {
        for (int j=0; j<plateauPartie->getTaille(); j++)
        {
            if (plateauPartie->getCaracteristic(i,j)== 1 && (plateauPartie->getAppartenance(i,j)== idJoueurTour))
                {
                    tabJoueur[idJoueurTour]->ajoutArgent(10);
                }
        }
    }
}

void Partie::finTour()
{
    plateauPartie->enleverUniteMorte();
    plateauPartie->setFauxAEteJouerPlateau();
    plateauPartie->mettreAJourAppartenanceMaison();
    impotJoueur();
    idJoueurTour++;
    if (idJoueurTour>=nbJoueur)
    {
        idJoueurTour=0;
    }
}

void Partie::sauvgarderPartie(std::string nomF)
{
    assert(!nomCorrect("listePartieEnregistree.txt", nomF+".txt"));
	ofstream fichierPartie("./data/listePartieEnregistree.txt", ios::app);
    assert (fichierPartie.is_open());
    fichierPartie << nomF<<".txt"<<endl;
    fichierPartie.close();

    string const cheminFichierTxt= "./data/sauv/"+nomF+".txt";
    ofstream fichier(cheminFichierTxt.c_str());
    assert (fichier.is_open());
    fichier<<idJoueurTour<<endl;
    for (int i=0;i<nbJoueur;i++)
    {
        fichier<<tabJoueur[i]->getNom()<<" "<<tabJoueur[i]->getArgent()<<endl;
    }
    fichier<<"plateau:"<<endl;
    fichier.close();
    plateauPartie->sauvgarderPlateau(cheminFichierTxt);
}
