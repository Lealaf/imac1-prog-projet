#ifndef PARTIE_H
#define PARTIE_H

#include <string>

#include "Joueur.h"
#include "Plateau.h"

const int MAXJOUEUR= 10;
const int NBUNITE= 4;

class Partie
{
    private:
        int nbJoueur;
        Joueur* tabJoueur [MAXJOUEUR];
        Plateau* plateauPartie;
        int idJoueurTour;
        bool continuerPartie;

        bool joueurAPerdu(int idJoueur);
        void impotJoueur();

    public:
        Partie(std::string nomListeTxt, int num);
        virtual ~Partie();
        int tabUnitePrix[NBUNITE];


        //fct sur joureur
        int getIdJoueurTour();
        Joueur* getJoueur(int idJ);

        //fct sur etat partie
        bool getContinuerPartie();
        void setContinuerPartie(bool newBool);
        int joueurGagnant();
        void finTour();
        void sauvgarderPartie(std::string nomF);

        //fct sur plateau
        Plateau* getPlateau();

        //fct possibilite/action joueur
        bool achatUnitePossible(int pX, int pY,int idUnite);
        bool acheterUnite(int pX, int pY,int idUnite);
        bool peutJouerUnite (int IdJ);





};

#endif // PARTIE_H
