#include "Unite.h"

using namespace std;

Unite::~Unite()
{
    //dtor
}

Unite::Unite(int newIdJoueur, int newIdFamille)
{
    idJoueur = newIdJoueur;
    aDeplace=false;
    aAttaque=false;
    idFamille=newIdFamille;
    switch (idFamille){
    case 0: //bug : expected : ;
        ptVieInitiale = 10;
        ptVie = 10;
        force = 10;
        defence = 2;
        porte = 1;
        mobilite = 1;
        break;
    case 1://bug : not declered in this scope
        ptVieInitiale = 10;
        ptVie = 10;
        force = 10;
        defence = 1;
        porte = 1;
        mobilite = 2;
        break;
    case 2: //bug : conversion type
        ptVieInitiale = 10;
        ptVie = 10;
        force = 10;
        defence = 1;
        porte = 2;
        mobilite = 1;
        break;
    case 3: //bug : segmentation fault
        ptVieInitiale = 10;
        ptVie = 10;
        force = 10;
        defence = 0;
        porte = 3;
        mobilite = 1;
        break;
    default:
        assert(false);
    }

}

Unite::Unite(int newIdJoueur, int newIdFamille, float newPtVie)
{

    idJoueur = newIdJoueur;
    aDeplace=false;
    aAttaque=false;
    idFamille=newIdFamille;
    ptVie = newPtVie;
    switch (idFamille){
    case 0: //bug : expected : ;
        ptVieInitiale = 10;
        force = 10;
        defence = 2;
        porte = 1;
        mobilite = 1;
        break;
    case 1://bug : conversion type
        ptVieInitiale = 10;
        force = 10;
        defence = 1;
        porte = 1;
        mobilite = 2;
        break;
    case 2: //bug : conversion type
        ptVieInitiale = 10;
        force = 10;
        defence = 1;
        porte = 2;
        mobilite = 1;
        break;
    case 3: //bug : segmentation fault
        ptVieInitiale = 10;
        force = 10;
        defence = 0;
        porte = 3;
        mobilite = 1;
        break;
    default:
        assert(false);
    }
}



int Unite::getIdJoueur()
{
    return idJoueur;
}

bool Unite::getADeplace()
{
    return aDeplace;
}

bool Unite::getAAttaque()
{
    return aAttaque;
}


int Unite::getIdFamille()
{
    return idFamille;
}

float Unite::getPtInitiale()
{
    return ptVieInitiale;
}

float Unite::getPtVie()
{
    return ptVie;
}

float Unite::getForce()
{
    return force;
}

int Unite::getDefence()
{
    return defence;
}

int Unite::getPorte()
{
    return porte;
}

int Unite::getMobilite()
{
    return mobilite;
}

void Unite::setADeplace(bool newBool)
{
    aDeplace= newBool;
}

void Unite::setAAttaque(bool newBool)
{
    aAttaque=newBool;
}


void Unite::setPvRecoisAttaque(Unite* uniteAtt)
{
    ptVie= ptVie - (uniteAtt->getForce()*uniteAtt->getPtVie()/uniteAtt->getPtInitiale())/defence;
}

