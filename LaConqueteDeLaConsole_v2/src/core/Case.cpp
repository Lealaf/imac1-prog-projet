#include "Case.h"

using namespace std;

Case::Case()
{
    caracteristic = 0;
    appartenance = -1;
    uniteCase = NULL;
}

Case::Case(int idcarac)
{
    caracteristic = idcarac;
    appartenance = -1;
    uniteCase = NULL;
}

Case::Case(int idcarac, int idJouAppartenace)
{
    caracteristic = idcarac;
    appartenance = idJouAppartenace;
    uniteCase = NULL;
}

Case::~Case()
{
    caracteristic = 0;
    appartenance = 0;
    delete uniteCase;
    uniteCase=NULL;
}

int Case::getCaracteristic()
{
    return caracteristic;
}

int Case::getAppartenance()
{
    return appartenance;
}

void Case::setAppartenance(int newApp)
{
    appartenance = newApp;
}

bool Case::ilyaUneUnite()
{
    if (uniteCase != NULL)
    {
        return true;
    }else
    {
        return false;
    }
}

Unite* Case::getUniteCase()
{
    assert(ilyaUneUnite());
    return uniteCase;
}

void Case::deleteUniteCase()
{
    assert(ilyaUneUnite());
    delete uniteCase;
    uniteCase = NULL;
}

void Case::changerPointeurUniteCase(Case* ancienCaseUnite)
{
    assert(!ilyaUneUnite());
    assert(ancienCaseUnite->ilyaUneUnite());
    uniteCase = ancienCaseUnite->getUniteCase();
    ancienCaseUnite->setPointeurUniterNull();
}

void Case::ajoutUnite(int idJoueur, int idFamille)
{
    assert(!ilyaUneUnite());
    uniteCase = new Unite (idJoueur, idFamille);
}

void Case::ajoutUnitePV(int idJoueur, int idFamille, float ptVie)
{
    assert(!ilyaUneUnite());
    uniteCase = new Unite (idJoueur, idFamille,ptVie);
}

void Case::setPointeurUniterNull()
{
    uniteCase =NULL;
}
