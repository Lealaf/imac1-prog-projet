#include "Plateau.h"

using namespace std;

Plateau::Plateau(std::string cheminFichierTxt)
{
    string const cheminFichierTxtConst=cheminFichierTxt;
    ifstream fichier(cheminFichierTxtConst.c_str());  //Ouverture d'un fichier en lecture
    assert(fichier.is_open());

    string mot;
    while(fichier >> mot && mot != "plateau:") //Tant qu'on n'est pas � la fin, on lit
    {
        //juste pour arriver au bon endroit
    }
    fichier>>taille; //On lit un mot

    int i=0;
    int j=0;
    while(fichier >> mot && mot != "unite:") //Tant qu'on n'est pas � la fin, on lit
    {
        if (mot=="|")
        {
            j++;
            i=0;
        }else
        {
            if (charToInt(mot[0])==1)
            {
                if (mot[1]=='-')
                {
                    tableau[i][j]=new Case(charToInt(mot[0]), -1);
                }else
                {
                    tableau[i][j]=new Case(charToInt(mot[0]), charToInt(mot[1]));
                }
            }else
            {
                tableau[i][j]=new Case(stringToInt(mot));
            }
            i++;
        }
    }
    string caracterePX;
    while(fichier >> caracterePX) //Tant qu'on n'est pas � la fin, on lit
    {
        string caracterePY;
        string idJoueur;
        string idFamille;
        string ptVie;

        fichier >> caracterePY>> idJoueur>> idFamille>> ptVie;
        int pX = stringToInt(caracterePX);
        int pY = stringToInt(caracterePY);
        assert(abs(pX)<taille);
        assert(abs(pY)<taille);
        assert(tableau [pX][pY]!=NULL);
        assert(!(tableau [pX][pY]->ilyaUneUnite()));
        assert(!estUnObstacle(pX, pY));
        if (ptVie== "-")
        {
            tableau [pX][pY]->ajoutUnite(stringToInt(idJoueur), stringToInt(idFamille));
        }else
        {
            tableau [pX][pY]->ajoutUnitePV(stringToInt(idJoueur), stringToInt(idFamille), stringToFloat(ptVie));
        }

    }
    fichier.close();
}


Plateau::~Plateau()
{
    for (int i=0; i<taille; i++)
    {
        for (int j=0; j<taille; j++)
        {
            delete tableau[i][j];
            tableau[i][j]= NULL;
        }
    }
    taille = 0;
}


int Plateau::getTaille()
{
    return taille;
}


int Plateau::getCaracteristic(int pX, int pY)
{
    assert(coordonnerExiste(pX, pY));
    return tableau[pX][pY]->getCaracteristic();
}

int Plateau::getAppartenance(int pX, int pY)
{
    assert(coordonnerExiste(pX, pY));
    return tableau[pX][pY]->getAppartenance();
}

void Plateau::setAppartenance(int pX, int pY, int idJoueur)
{
    assert(coordonnerExiste(pX, pY));
    tableau[pX][pY]->setAppartenance(idJoueur);
}

bool Plateau::estUnObstacle(int pX, int pY)
{
    int teste = getCaracteristic(pX,pY);
    if (getCaracteristic(pX,pY)== 2)
    {
        return true;
    }
    return false;
}

bool Plateau::coordonnerExiste(int pX, int pY)
{
    if ( pX >= 0 && pX < taille)
    {
        if (pY >= 0 && pY < taille)
        {
            return true;
        }
    }

    return false;
}

bool Plateau::uniteExiste(int pX, int pY)
{
    if (coordonnerExiste(pX, pY) && tableau[pX][pY]->ilyaUneUnite())
    {
        return true;
    }else
    {
        return false;
    }
}

Unite* Plateau::getUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase();

}

void Plateau::ajoutUnite(int pX, int pY, int idJoueur, int idFamille)
{
    assert(coordonnerExiste(pX, pY));
    assert(!uniteExiste(pX, pY));
    tableau[pX][pY]->ajoutUnite(idJoueur,idFamille);
    setADeplaceUnite(pX,pY,true);
    setAAttaqueUnite(pX,pY,true);
}

int Plateau::getIdJoueurUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase()->getIdJoueur();
}

bool Plateau::getADeplaceUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase()->getADeplace();
}

bool Plateau::getAAttaqueUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase()->getAAttaque();
}

int Plateau::getIdFamilleUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase()->getIdFamille();
}

float Plateau::getPtVieUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase()->getPtVie();
}

float Plateau::getForceUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase()->getForce();
}

int Plateau::getPorteUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase()->getPorte();
}

int Plateau::getMobiliteUnite(int pX, int pY)
{
    assert(uniteExiste(pX,pY));
    return tableau[pX][pY]->getUniteCase()->getMobilite();
}

void Plateau::setADeplaceUnite(int pX, int pY, bool newBool)
{
    assert(uniteExiste(pX,pY));
    tableau[pX][pY]->getUniteCase()->setADeplace(newBool);
}

void Plateau::setAAttaqueUnite(int pX, int pY, bool newBool)
{
    assert(uniteExiste(pX,pY));
    tableau[pX][pY]->getUniteCase()->setAAttaque(newBool);
}

void Plateau::setFauxAEteJouerPlateau()
{
    for (int i=0; i<taille; i++)
    {
        for (int j=0; j<taille; j++)
        {
            if (uniteExiste(i,j) && getADeplaceUnite(i,j))
                {
                    getUnite(i,j)->setADeplace(false);
                    getUnite(i,j)->setAAttaque(false);
                }
        }
    }
}

int Plateau::nbUniteJoueur(int idJoueur)
{
    int nbUnite=0;
    for (int i=0; i<taille; i++)
    {
        for (int j=0; j<taille; j++)
        {
            if (uniteExiste(i,j) && (getIdJoueurUnite(i,j)== idJoueur))
                {
                    nbUnite++;
                }
        }
    }
    return nbUnite;

}

bool Plateau::deplacementPossible(int pX, int pY, int newX, int newY)
{
    if( (coordonnerExiste(pX, pY)&& (coordonnerExiste(newX, newY))) //coordonner ds le plateau
       && uniteExiste(pX,pY) // l'uniter existe
       && (abs(pX-newX )<= getMobiliteUnite(pX, pY))
       && (abs(pY-newY )<= getMobiliteUnite(pX, pY))
       && !estUnObstacle(newX,newY)
       && !uniteExiste(newX,newY)
       )
    {
        return true;
    }else
    {
        return false;
    }
}

bool Plateau::deplacerUnite(int pX, int pY, int newX, int newY)
{
    if( deplacementPossible( pX, pY, newX, newY)){
        tableau[newX][newY]->changerPointeurUniteCase(tableau[pX][pY]);
        return true;
    }
    return false;


}

bool Plateau::uniteAporte(int pX, int pY)
{
    assert(coordonnerExiste(pX, pY));
    assert(uniteExiste(pX,pY));
    int porte = getPorteUnite(pX,pY);
    for (int i=pX-porte; i<=pX+porte; i++)
    {
        for (int j=pY-porte; j<=pY+porte; j++)
        {
            if (uniteExiste(i,j)&& getIdJoueurUnite(pX,pY)!=getIdJoueurUnite(i,j))
            {
                return true;
            }
        }
    }
    return false;
}

bool Plateau::attaquerPossible(int pXAttaquante, int pYAttaquante, int pXCible, int pYCible)
{
    if( uniteExiste(pXAttaquante,pYAttaquante) && uniteExiste(pXCible,pYCible)
       && (getIdJoueurUnite(pXAttaquante, pYAttaquante) != getIdJoueurUnite(pXCible, pYCible))
       && ( abs(pXAttaquante-pXCible)<= getPorteUnite(pXAttaquante,pYAttaquante))
       && ( abs(pYAttaquante-pYCible)<= getPorteUnite(pXAttaquante,pYAttaquante))
      )
    {
        return true;
    }else
    {
        return false;
    }
}

bool Plateau::attaquerUnite(int pXAttaquante, int pYAttaquante, int pXCible, int pYCible)
{

    if( attaquerPossible(pXAttaquante, pYAttaquante, pXCible, pYCible))
    {

        getUnite(pXCible,pYCible)->setPvRecoisAttaque(getUnite(pXAttaquante, pYAttaquante));
        if (getPtVieUnite(pXCible,pYCible) > 0){
            getUnite(pXAttaquante,pYAttaquante)->setPvRecoisAttaque(getUnite(pXCible, pYCible));
        }
        return true;
    }

    return false;

}


void Plateau::enleverUniteMorte()
{
    for (int i=0; i<taille; i++)
    {
        for (int j=0; j<taille; j++)
        {
            if (uniteExiste(i,j) && (getPtVieUnite(i,j) <= 0))
            {
               tableau[i][j]->deleteUniteCase();

            }
        }
    }
}

void Plateau::mettreAJourAppartenanceMaison()
{
    for (int i=0; i<taille; i++)
    {
        for (int j=0; j<taille; j++)
        {
            if (getCaracteristic(i,j)==1 && uniteExiste(i,j))
            {
                setAppartenance(i,j,getIdJoueurUnite(i,j));
            }
        }
    }
}

void Plateau::sauvgarderPlateau(std::string cheminFichierTxt)
{
    string const cheminFichierTxtConst= cheminFichierTxt;
    ofstream fichier(cheminFichierTxtConst.c_str(), ios::app);
    assert (fichier.is_open());
    fichier<<taille<<endl;
    for (int i=0; i<taille; i++)
    {
        for (int j=0; j<taille; j++)
        {
            if (tableau[i][j]->getCaracteristic()== 1)
            {
                fichier<<tableau[i][j]->getCaracteristic();
                if (tableau[i][j]->getAppartenance()== -1)
                {
                    fichier<<'-';
                }else
                {
                    fichier<<tableau[i][j]->getAppartenance();
                }
                fichier <<" ";
            }else
            {
                fichier<<tableau[i][j]->getCaracteristic();
                fichier <<" ";
            }

        }
        fichier <<"|"<<endl;
    }
    fichier <<"unite:"<<endl;
    for (int i=0; i<taille; i++)
    {
        for (int j=0; j<taille; j++)
        {
            if (uniteExiste(i,j))
            {
                fichier<<i<<" "<<j<<" "
                       <<getIdJoueurUnite(i, j)<<" "
                       <<getIdFamilleUnite(i, j)<< " "
                       <<getPtVieUnite(i, j)<< " "
                       << endl;
            }
        }
    }
    fichier.close();
}
