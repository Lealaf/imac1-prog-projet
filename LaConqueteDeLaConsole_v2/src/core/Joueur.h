#ifndef JOUEUR_H
#define JOUEUR_H

#include <string>

class Joueur
{
    private:
        std::string nom;
        int couleur;
        int argent;
        bool isBot;
    public:
        Joueur(std::string newNom, int newArgent);
        Joueur(std::string newNom, int newCouleur, int newArgent);
        virtual ~Joueur();

        std::string getNom();
        int getCouleur();
        int getArgent();
        void setArgent(int newArg);
        void ajoutArgent(int enPlus);
        void enleverArgent(int enMoins);
        bool getIsBot();
        void setIsBot(bool newBool);
};

#endif // JOUEUR_H
