#include "fonctionFichier.h"

using namespace std;


int stringToInt(std::string texte)
{
    int entier;
	istringstream iss( texte );
	iss>>entier;
	return entier;
}

int charToInt(char lettre)
{
    return (int)lettre-(int)'0';
}

float stringToFloat(std::string texte)
{
    float nb;
	istringstream iss( texte );
	iss>>nb;
	return nb;
}

bool nomCorrect(std::string nomListeTxt, std::string const nomFichierTxt)
{

    for(int i=0; i < nomFichierTxt.length(); i++)
    {
        if( nomFichierTxt[i] == ' ')
        {
                return false;
        }
    }

    string const nomL = "./data/" + nomListeTxt;
    ifstream fichierPartieLecture(nomL.c_str());
    assert (fichierPartieLecture.is_open());

    string ligne;
    while ( std::getline( fichierPartieLecture, ligne ) )
    {
        if (nomFichierTxt == ligne)
        {
            fichierPartieLecture.close();
            return true;
        }
    }
    fichierPartieLecture.close();
    return false;
}

std::string nomNumeroFichierTxt(std::string nomListeTxt, int num)
{
    string const nomListePE ="./data/"+nomListeTxt;
    ifstream fichierListePE(nomListePE.c_str());  //Ouverture d'un fichier en lecture
    assert (fichierListePE.is_open());

    string ligne;
    int conteur=0;

    while ( std::getline( fichierListePE, ligne ) )
    {
        if (conteur == num)
        {
            fichierListePE.close();
            return ligne;
        }
        conteur++;

    }
    fichierListePE.close();
    assert(1); // si on arrive ici c'est que la ligne n'existe pas
}

int nbLigneFichier(std::string nomListeTxt)
{
    string const nomLTxt = "./data/"+nomListeTxt;
    ifstream fichier(nomLTxt.c_str());
    assert (fichier.is_open());

    string ligne;
    int nombrePlateau=0;
    while ( std::getline( fichier, ligne ) )
    {
        nombrePlateau++;
    }
    fichier.close();
    return nombrePlateau;
}
