#include "Joueur.h"

using namespace std;

Joueur::Joueur(std::string newNom, int newArgent)
{
    nom = newNom;
    argent = newArgent;
    isBot= false;
}

Joueur::Joueur(std::string newNom, int newCouleur, int newArgent)
{
    nom = newNom;
    couleur = newCouleur;
    argent = newArgent;
}

Joueur::~Joueur()
{
    //dtor
}

string Joueur::getNom()
{
    return nom;
}

int Joueur::getCouleur()
{
    return couleur;
}

int Joueur::getArgent()
{
    return argent;
}

void Joueur::setArgent(int newArg)
{
    argent = newArg;
}

void Joueur::ajoutArgent(int enPlus)
{
    argent = argent + enPlus;
}

void Joueur::enleverArgent(int enMoins)
{
    argent = argent - enMoins;
}

bool Joueur::getIsBot()
{
    return isBot;
}

void Joueur::setIsBot(bool newBool)
{
    isBot = newBool;
}
