#ifndef UNITE_H
#define UNITE_H

#include <assert.h>


class Unite
{
    private:
        int idJoueur;
        bool aDeplace;
        bool aAttaque;
        int idFamille;
        float ptVieInitiale;
        float ptVie;
        float force;
        int defence;
        int porte; //port� de l'attaque
        int mobilite; //TODO mobiliter

    public:

        Unite(int newIdJoueur, int newIdFamille);
        Unite(int newIdJoueur, int newIdFamille, float newPtVie);
        ~Unite();

        //normalement ne sret qu'a l'affichage
        int getIdJoueur();
        bool getADeplace();
        bool getAAttaque();
        int getIdFamille();
        float getPtInitiale();
        float getPtVie();
        float getForce();
        int getDefence();
        int getPorte();
        int getMobilite();

        //sert a d'autre fct
        void setADeplace(bool newBool);
        void setAAttaque(bool newBool);
        void setPvRecoisAttaque(Unite* uniteAtt);

};

#endif // UNITE_H
