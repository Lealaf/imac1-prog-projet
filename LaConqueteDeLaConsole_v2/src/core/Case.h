#ifndef CASE_H
#define CASE_H

#include <assert.h>
#include <stdlib.h>

#include "Unite.h"

class Case
{
    private:
        int caracteristic; //0 = rien de spe  1= ville  2 obstacle
        int appartenance; //si cette case appartien a un joueur
        Unite * uniteCase;


    public:
        Case();
        Case(int idcarac);
        Case(int idcarac, int idJouAppartenace);
        virtual ~Case();

        int getCaracteristic();
        int getAppartenance();
        void setAppartenance(int newApp);
        Unite * getUniteCase();
        bool ilyaUneUnite ();
        void deleteUniteCase();
        void changerPointeurUniteCase(Case* ancienCaseUnite);
        void ajoutUnite (int idJoueur, int idFamille);
        void ajoutUnitePV(int idJoueur, int idFamille, float ptVie);
        void setPointeurUniterNull();
};

#endif // CASE_H
