#ifndef PLATEAU_H
#define PLATEAU_H

#include <fstream> //pour la gestion de fichiers
#include <assert.h>
#include <stdlib.h> // pour abs (val absolu)
#include <string>
#include <iostream>


#include "fonctionFichier.h"
#include "Case.h"
#include "Unite.h"

const int MAXGRILLE= 100;

class Plateau
{
    private:
        Case* tableau [MAXGRILLE][MAXGRILLE];
        int taille;


        void setAppartenance(int pX, int pY, int idJoueur);

    public:
        Plateau(std::string cheminFichierTxt);

        virtual ~Plateau();


        int getTaille();

        //fct sur case
        bool coordonnerExiste(int pX, int pY);
        int getCaracteristic(int pX, int pY);
        int getAppartenance(int pX, int pY);
        bool estUnObstacle (int pX, int pY);

        //fci sur uniter
        bool uniteExiste (int pX, int pY);
        Unite* getUnite(int pX, int pY);
        void ajoutUnite (int pX, int pY, int idJoueur, int idFamille);

        int getIdJoueurUnite(int pX, int pY);
        bool getADeplaceUnite (int pX, int pY);
        bool getAAttaqueUnite (int pX, int pY);
        int getIdFamilleUnite(int pX, int pY);
        float getPtVieUnite(int pX, int pY);
        float getForceUnite(int pX, int pY);
        int getPorteUnite(int pX, int pY);
        int getMobiliteUnite(int pX, int pY);

        void setAEteJouerUnite (int pX, int pY, bool newBool);
        void setADeplaceUnite (int pX, int pY, bool newBool);
        void setAAttaqueUnite (int pX, int pY, bool newBool);
        int nbUniteJoueur (int idJoueur);

        //fct action unite
        bool deplacementPossible (int pX, int pY, int newX, int newY);
        bool deplacerUnite (int pX, int pY, int newX, int newY);
        bool uniteAporte (int pX, int pY); //permet de savoir si l'unite peut attaquer (si il y a une unite dans sa porter)
        bool attaquerPossible (int pXAttaquante, int pYAttaquante, int pXCible, int pYCible);
        bool attaquerUnite (int pXAttaquante, int pYAttaquante, int pXCible, int pYCible);

        //fct gestion plateau
        void setFauxAEteJouerPlateau(); //met uniteAEteJouer a faux pour tout le plateau
        void enleverUniteMorte(); // enl�ve tout les cadavres du lateau
        void mettreAJourAppartenanceMaison ();

        void sauvgarderPlateau( std::string cheminFichierTxt);
};

#endif // PLATEAU_H
