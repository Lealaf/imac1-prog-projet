#ifndef FONCTIONFICHIER_H
#define FONCTIONFICHIER_H

#include <sstream>
#include <iostream>
#include <fstream> //pour la gestion de fichiers
#include <assert.h>


int stringToInt(std::string texte);
int charToInt(char lettre);
float stringToFloat(std::string texte);

bool nomCorrect(std::string nomListeTxt, std::string const nomFichierTxt); //verifi si le nom du fichier existe dans la liste
std::string nomNumeroFichierTxt (std::string nomListeTxt, int num); //retourne le nom du fichier sur la ligne num
int nbLigneFichier(std::string nomListeTxt);

#endif // FONCTIONFICHIER_H
