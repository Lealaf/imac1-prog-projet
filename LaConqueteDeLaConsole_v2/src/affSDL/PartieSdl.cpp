#include "PartieSdl.h"

// PartieSDL
bool PartieSDL(bool dejaJouee){
    // Ouverture d'une fenêtre SDL
    // Chargement du plateau
    Plateau* plateau_jeu;
    // Initialisation de la SDL
    if(dejaJouee!=1){
        initialisationSDL("La_Conquete_de_la_Console");
        debutDessin();

        // Active les textures une seule fois
        activeTexture();
    }

    int actionMenu;
    int gagnant;
    int rejouer;

    // Activation du Dessin
    actionMenu = debutJeu();

    if(actionMenu == 1){
        int num = choisirPartie();
        if(num!=-1){
            Partie* part1 = new Partie("listeFichier.txt",num);
            plateau_jeu = part1->getPlateau();
            TAILLE_BLOC = screen->h/plateau_jeu->getTaille();
            if(mainPlaySolo(plateau_jeu,part1) != -1){
                if(finJeu(gagnant)==1){
                    PartieSDL(1);
                }
            }
        }
    }else if(actionMenu == 2){
        int num = choisirPartie();
        if(num!=-1){
            Partie* part1 = new Partie("listeFichier.txt",num);
            plateau_jeu = part1->getPlateau();
            TAILLE_BLOC = screen->h/plateau_jeu->getTaille();
            if(mainPlayMulti(plateau_jeu,part1) != -1){
                if(finJeu(gagnant)==1){
                    PartieSDL(1);
                }
            }
        }
    }else if(actionMenu==3){
        int num = chargerPartie();
        if(num!=-1){
            Partie* part1 = new Partie("listePartieEnregistree.txt",num);
            plateau_jeu = part1->getPlateau();
            TAILLE_BLOC = screen->h/plateau_jeu->getTaille();
            if(mainPlaySolo(plateau_jeu,part1) != -1){
                if(finJeu(gagnant)==1){
                    PartieSDL(1);
                }
            }
        }
    }else if(actionMenu==4){
        affichageIntroSDL();
        PartieSDL(1);
    }

    // Desactivation du Dessin
    finDessin();
    disabledGL();
    desactiveTexture();

    // Fin de SDL
    SDL_Quit();
    return EXIT_SUCCESS;
}

// Renvoie un nombre en fonction du type de bouton
int debutJeu(){
    // Affichage Menu
    bool menu = true;
    int loop = 1;

    affichageMenuDebut();

    // Tant que le joueur est sur le menu :
    while(menu&&loop){
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT)
			{
				loop = 0;
				return -1;
				break;
			}

            switch(e.type)
            {

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    if(e.button.x>=(screen->w)/3 && e.button.x<=2*(screen->w)/3){
                        // Renvoie 1 si jeu solo
                        if(e.button.y>=(4*(screen->h)/8-20) && e.button.y<=(5*(screen->h)/8-20))
                        {
                            menu=false;
                            return(1);
                            break;
                        // Renvoie 2 si jeu multi
                        }else if(e.button.y>=5*(screen->h)/8-20 && e.button.y<=6*(screen->h)/8-20)
                        {
                            menu=false;
                            return(2);
                            break;
                        // Renvoie 3 si continuer
                        }else if(e.button.y>=6*(screen->h)/8-20 && e.button.y<=7*(screen->h)/8-20)
                        {
                            menu=false;
                            return(3);
                            break;
                        // Renvoie 4 si info
                        }else if(e.button.y>=7*(screen->h)/8-20 && e.button.y<=8*(screen->h)/8-20)
                        {
                            menu=false;
                            return(4);
                            break;
                        }
                    }
                    break;

                default:
                    break;
            }
        }
        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }
}

int finJeu(int id_joueur_gagnant){
    // Affichage du gagnant
    affichageMenuFin(id_joueur_gagnant);

    // Tant que le joueur est sur le menu :
    int loop = 1;
    while(loop){
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT)
			{
				loop = 0;
				break;
			}

            switch(e.type)
            {

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    if(e.button.y>=(13.5*screen->h)/16 && e.button.y<=(15.5*screen->h)/16)
                    {
                        if(e.button.x>=0 && e.button.x<=(screen->w/2))
                        {
                            return 1;
                            break;
                        }else if(e.button.x>=(screen->w/2) && e.button.x<=screen->w)
                        {
                            return 0;
                            break;
                        }
                    }
                    break;

                default:
                    break;
            }
        }
        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }

    }
}

int choisirPartie(){
    Plateau* demo = new Plateau("data/plateau/Domination.txt");
    TAILLE_BLOC = screen->h/(demo->getTaille()-2);
    afficherUnite(screen,demo);

    glColor4f(255,255,255,1);
    // Affichage texte
    SDL_Color couleur = {250,250,250};
    TTF_Font* fontTitre = TTF_OpenFont("FORCEDSQUARE.ttf", 50);
    if (!fontTitre)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }

    SDL_Rect pos;
    pos.x = 150;
    pos.y = 60;

    SDL_GL_RenderText("Quel plateau voulez-vous charger ?", fontTitre, couleur,pos);

    TTF_Font* font = TTF_OpenFont("FORCEDSQUARE.ttf", 40);

    pos.x = 450;
    pos.y = 200;
    std::string nomF = nomNumeroFichierTxt("listeFichier.txt", 0);
    SDL_GL_RenderText(nomF.c_str(),font, couleur,pos);

    pos.x = 450;
    pos.y = 350;
    nomF = nomNumeroFichierTxt("listeFichier.txt", 1);
    SDL_GL_RenderText(nomF.c_str(),font, couleur,pos);

    pos.x = 450;
    pos.y = 500;
    nomF = nomNumeroFichierTxt("listeFichier.txt", 2);
    SDL_GL_RenderText(nomF.c_str(),font, couleur,pos);

    pos.x = 450;
    pos.y = 650;
    nomF = nomNumeroFichierTxt("listeFichier.txt", 3);
    SDL_GL_RenderText(nomF.c_str(),font, couleur,pos);

    refreshSDL();
    int loop = 1;
    while(loop){
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT)
			{
				loop = 0;
				break;
			}

            switch(e.type)
            {

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    if(e.button.x>=450 && e.button.y<=670)
                    {
                        if(e.button.y>=200 && e.button.y<=270)
                        {
                            return 0;
                            break;
                        }else if(e.button.y>=350 && e.button.y<=420)
                        {
                            return 1;
                            break;
                        }else if(e.button.y>=500 && e.button.y<=570){
                            return 2;
                            break;
                        }else if(e.button.y>=650 && e.button.y<=720){
                            return 3;
                            break;
                        }else{
                            return -1;
                            break;
                        }
                    }
                    break;

                default:
                    break;
            }
        }
        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }

    }
    return -1;
}

int chargerPartie(){
    Plateau* demo = new Plateau("data/plateau/Domination.txt");
    TAILLE_BLOC = screen->h/(demo->getTaille()-2);
    afficherUnite(screen,demo);

    glColor4f(255,255,255,1);
    // Affichage texte
    SDL_Color couleur = {250,250,250};
    TTF_Font* fontTitre = TTF_OpenFont("FORCEDSQUARE.ttf", 50);
    if (!fontTitre)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }

    SDL_Rect pos;
    pos.x = 150;
    pos.y = 60;

    SDL_GL_RenderText("Quel partie voulez-vous reprendre ?", fontTitre, couleur,pos);

    TTF_Font* font = TTF_OpenFont("FORCEDSQUARE.ttf", 40);

    std::string nomF;

    if(nbLigneFichier("listePartieEnregistree.txt")>=1){
        pos.x = 450;
        pos.y = 200;
        nomF = nomNumeroFichierTxt("listePartieEnregistree.txt", 0);
        SDL_GL_RenderText(nomF.c_str(),font, couleur,pos);
    }

    if(nbLigneFichier("listePartieEnregistree.txt")>=2){
        pos.x = 450;
        pos.y = 350;
        nomF = nomNumeroFichierTxt("listePartieEnregistree.txt", 1);
        SDL_GL_RenderText(nomF.c_str(),font, couleur,pos);
    }

    if(nbLigneFichier("listePartieEnregistree.txt")>=3){
        pos.x = 450;
        pos.y = 500;
        nomF = nomNumeroFichierTxt("listePartieEnregistree.txt", 2);
        SDL_GL_RenderText(nomF.c_str(),font, couleur,pos);
    }

    if(nbLigneFichier("listePartieEnregistree.txt")>=4){
        pos.x = 450;
        pos.y = 650;
        nomF = nomNumeroFichierTxt("listePartieEnregistree.txt", 3);
        SDL_GL_RenderText(nomF.c_str(),font, couleur,pos);
    }

    refreshSDL();
    int loop = 1;
    while(loop){
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT)
			{
				loop = 0;
				break;
			}

            switch(e.type)
            {

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    if(e.button.x>=450 && e.button.y<=670)
                    {
                        if(e.button.y>=200 && e.button.y<=270 && nbLigneFichier("listePartieEnregistree.txt")>=1)
                        {
                            return 0;
                            break;
                        }else if(e.button.y>=350 && e.button.y<=420 && nbLigneFichier("listePartieEnregistree.txt")>=2)
                        {
                            return 1;
                            break;
                        }else if(e.button.y>=500 && e.button.y<=570 && nbLigneFichier("listePartieEnregistree.txt")>=3){
                            return 2;
                            break;
                        }else if(e.button.y>=650 && e.button.y<=720 && nbLigneFichier("listePartieEnregistree.txt")>=4){
                            return 3;
                            break;
                        }else{
                            return -1;
                            break;
                        }
                    }
                    break;

                default:
                    break;
            }
        }
        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }

    }
    return -1;}
