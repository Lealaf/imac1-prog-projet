/*
affichagePlateau.h
rôle : affiche le plateau de jeu à partir des fichier.txt
*/

#ifndef SDLTOOLS_H
#define SDLTOOLS_H

/*Bibliotheque SDL */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>

//#include <windows.h>
#include <Joueur.h>
#include <Plateau.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

static float aspectRatio;

/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE = 150.;

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

/* TAILLE D'UN BLOC */
extern int TAILLE_BLOC;

/* Déclaration de variable */
extern SDL_Surface* screen;
extern int WINDOW_WIDTH;
extern int WINDOW_HEIGHT;
extern GLuint texture_Magasin;

// Définition de la police
extern SDL_Color couleurPolice;
extern SDL_Color couleurJoueur1;
extern SDL_Color couleurJoueur2;

/* Initialisation de la SDL*/
void initialisationSDL(const char* windowTitle);
void reshape(SDL_Surface** surface, unsigned int width, unsigned int height);
void refreshSDL();
void chargerTexture(const char* filename,GLuint texture);

/* Fonction de Dessin */
void debutDessin();
void affichageBoutonMenu(GLuint textures,SDL_Surface* screen, int j);

void quads_texture(GLuint textures,SDL_Surface* screen,int i, int j,int z);
void attaque_quads(GLuint textures, bool left);
void quad_attaque(GLuint texture,int i, int j);

void quads_magasin(GLuint textures,SDL_Surface* screen,int i, int j);
void logo_magasin();

void affichageBoutonFin(GLuint textures,SDL_Surface* screen, int i);
void finDessin();

/* Fin SDL */
void libererTexture(GLuint textures);
void libererImg(SDL_Surface* surface);
void disabledGL();
void exitSDL();

/* SDL_TTF */
void chargerPolice(int taille, TTF_Font* police);
void afficherTexte(const char* texte, SDL_Color couleur,SDL_Rect position);


#endif // SDLTOOLS_H
