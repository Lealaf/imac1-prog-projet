/*
affichageTexte.h
rôle : affiche le plateau de jeu à partir des fichier.txt
*/

#ifndef AFFICHAGETEXTE_H
#define AFFICHAGETEXTE_H

/*Bibliotheque SDL */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>

//#include <windows.h>
#include <sdlTools.h>
#include <Joueur.h>
#include <Plateau.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void SDL_GL_RenderText(const char *text, TTF_Font *font, SDL_Color color,SDL_Rect &rectPos);

void affichageIntroSDL();

void texteOnglet(bool selected, int x, int y, Joueur* joueur_current, Plateau* plateau_jeu);

void affichageTexteMagasin(Joueur* joueur_current,int tabUnitePrix[]);

void affichage_PV(bool bot);

#endif //

