/*
PartieSdl.h
rôle : affiche le plateau de jeu à partir des fichier.txt
*/

#ifndef PARTIESDL_H
#define PARTIESDL_H

/*Bibliotheque SDL */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>

//#include <windows.h>
#include <assert.h>
#include <affichageTexte.h>
#include <Plateau.h>
#include <Partie.h>
#include <sdlTools.h>
#include <affichagePlateau.h>
#include <affichageJeu.h>
#include <sdlMove.h>
#include <mainPlay.h>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <Joueur.h>

// PartieSDL
bool PartieSDL(bool dejaJouee);

// Menu d'accueil : Renvoie un nombre en fonction du bouton cliqué
int debutJeu();

// Affichage de fin
int finJeu(int id_joueur_gagnant);

// Affichage choisirPartie
int choisirPartie();

// Affichage chargerPartie
int chargerPartie();


#endif // PARTIESDL_H
