/*
affichagePlateau.h

r�le : affiche le plateau de jeu � partir des fichier.txt
*/

#ifndef AFFICHAGEPLATEAU_H
#define AFFICHAGEPLATEAU_H

/* Bibliotheque SDL */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>

//#include <windows.h>
#include <Plateau.h>
#include <sdlTools.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// Textures Menu et Pause
extern GLuint texture_pause;

// Textures des 0 et 1
extern GLuint texture_fond_1;
extern GLuint texture_fond_2;

// Textures des unit�s Selected
extern GLuint texture_selection_V;
extern GLuint texture_selection_R;

// Textures des unit�s CanBeSelected
extern GLuint texture_CanBeSelected_R;
extern GLuint texture_CanBeSelected_V;

// Texture attack
extern GLuint texture_Attack;

// Texture des unit�s pouvant �tre attaqu�
extern GLuint texture_CanBeAttacked_R;
extern GLuint texture_CanBeAttacked_V;

// Texture croix
extern GLuint texture_croix;

// Texture Joueur
extern GLuint texture_joueur_1;
extern GLuint texture_joueur_2;

// Textures des ordinateurs
extern GLuint ordi_noir;
extern GLuint ordi_rouge;
extern GLuint ordi_violet;
extern GLuint obstacle;

// Textures des Virus de face
extern GLuint texture_VR_1;
extern GLuint texture_VR_2;
extern GLuint texture_VR_3;
extern GLuint texture_VV_1;
extern GLuint texture_VV_2;
extern GLuint texture_VV_3;

// Textures des Virus de dos
extern GLuint texture_VV_1_D;
extern GLuint texture_VV_2_D;
extern GLuint texture_VV_3_D;

// Textures des Virus de Profil
extern GLuint texture_VR_1_D;
extern GLuint texture_VR_2_D;
extern GLuint texture_VR_3_D;
extern GLuint texture_VV_1_G;
extern GLuint texture_VV_2_G;
extern GLuint texture_VV_3_G;

// Magasin et Menu
extern GLuint texture_Menu;
extern GLuint texture_Bravo;

// Bouton
extern GLuint texture_Bouton_Solo;
extern GLuint texture_Bouton_Multi;
extern GLuint texture_Bouton_Info;
extern GLuint texture_Bouton_Continuer;
extern GLuint texture_Bouton_Quitter;
extern GLuint texture_Bouton_Rejouer;
extern GLuint texture_bouton_Fin;

// Affichage Texture
void activeTexture();
void desactiveTexture();

// Affichage Plateau
void afficherFond(SDL_Surface* screen, Plateau* Plateau);
void afficherUnite(SDL_Surface* screen, Plateau* Plateau);
void affichagePlateau(Plateau* plateau_jeu);

#endif // AFFICHAGEPLATEAU_H

