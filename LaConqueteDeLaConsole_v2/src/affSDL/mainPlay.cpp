#include <mainPlay.h>

// Renvoie l'id du joueur gagant
int mainPlayMulti(Plateau* plateau_jeu,Partie* part1){
    // On sort du Menu et on rentre dans le jeu
    //Booléen qui permet la pause du jeu
    bool pause = false;
    // Boléen qui permlet l'ouverture du magasin
    bool magasin = false;

    // Permet de voir l'unité sélectionné
    bool selected = false;
    int selected_x;
    int selected_y;

    int id_joueur=part1->getIdJoueurTour();

    // Affichage du joueur qui commence
    afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
    affichagePlateau(plateau_jeu);
    affichageTour(id_joueur,plateau_jeu,part1);

    afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
    affichagePlateau(plateau_jeu);
    refreshSDL();

    /* Boucle principale du jeu */
    int loop =1;
    while(loop)
    {
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT)
			{
				loop = 0;
				break;
			}

			if(	e.type == SDL_KEYDOWN )
			{
                if(e.key.keysym.sym == SDLK_RETURN)
                {
                    if(pause == false){
                        menuPause(plateau_jeu);
                        pause = true;
                        break;
                    }else{
                        affichageSauvegarde(part1);
                        loop = 0;
                        break;
                    }
                }else if(e.key.keysym.sym == SDLK_ESCAPE)
                {
                        afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                        affichagePlateau(plateau_jeu);
                        refreshSDL();
                        pause=false;
                        break;
                }
			}

            /* Quelques exemples de traitement d'evenements : */
            switch(e.type)
            {

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    if(magasin==false){
                        if(selected==false){
                            if(clickUnite(e.button.x, e.button.y, plateau_jeu,id_joueur,part1)==1){
                                selected_x = (int) e.button.x/TAILLE_BLOC;
                                selected_y = (int) e.button.y/TAILLE_BLOC;
                                selected = true;
                                break;
                            }else if(e.button.x> plateau_jeu->getTaille()*TAILLE_BLOC && e.button.x< screen->w && e.button.y> 10*screen->h/12 && e.button.y< 11.5*screen->h/12){
                                part1->finTour();
                                id_joueur=part1->getIdJoueurTour();
                                // Affichage du Tour
                                affichageTour(id_joueur,plateau_jeu,part1);
                                afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                                affichagePlateau(plateau_jeu);
                                refreshSDL();
                                break;
                            }else{
                                break;
                            }
                        }else{
                            if(plateau_jeu->attaquerPossible(selected_x,selected_y,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC) && !plateau_jeu->getAAttaqueUnite(selected_x,selected_y)){
                                attaqueUnite(selected_x,selected_y,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC,plateau_jeu,false,part1);
                                selected = false;

                                // Si il n'y a plus de joueur on sort de la loop
                                if(plateau_jeu->nbUniteJoueur(0) ==0 || plateau_jeu->nbUniteJoueur(1)==0){
                                    loop = 0;
                                    break;
                                }

                                if(!part1->peutJouerUnite(id_joueur)){
                                    part1->finTour();
                                    id_joueur=part1->getIdJoueurTour();
                                    // Affichage du Tour
                                    affichageTour(id_joueur,plateau_jeu,part1);
                                }
                                afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                                affichagePlateau(plateau_jeu);
                                refreshSDL();
                                break;
                            }else if(plateau_jeu->deplacementPossible(selected_x,selected_y,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC) && !plateau_jeu->getADeplaceUnite(selected_x,selected_y) && !plateau_jeu->getAAttaqueUnite(selected_x,selected_y))
                            {
                                moveUnite(selected_x,selected_y,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC,plateau_jeu,part1);
                                selected = false;

                                if(plateau_jeu->getCaracteristic(e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC)){
                                    if(plateau_jeu->getAppartenance(e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC)==id_joueur ){
                                        affichageMagasin(id_joueur,plateau_jeu,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC,part1->getJoueur(id_joueur),part1->tabUnitePrix);
                                        magasin = true;
                                        break;
                                    }
                                }

                                if(!part1->peutJouerUnite(id_joueur)){
                                    part1->finTour();
                                    id_joueur=part1->getIdJoueurTour();
                                    // Affichage du Tour
                                    affichageTour(id_joueur,plateau_jeu,part1);
                                }
                                afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                                affichagePlateau(plateau_jeu);
                                refreshSDL();
                                break;
                            }else{
                                afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                                affichagePlateau(plateau_jeu);
                                refreshSDL();
                                selected = false;
                                break;
                            }
                        }
                    }else{
                        acheterUnite(plateau_jeu,id_joueur,e.button.x,e.button.y,part1->getJoueur(id_joueur),part1->tabUnitePrix);
                        afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                        affichagePlateau(plateau_jeu);
                        refreshSDL();

                        // On ferme le magasin
                        magasin = false;

                        if(!part1->peutJouerUnite(id_joueur)){
                            part1->finTour();
                            id_joueur=part1->getIdJoueurTour();
                            // Affichage du Tour
                            affichageTour(id_joueur,plateau_jeu,part1);
                        }
                        afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                        affichagePlateau(plateau_jeu);
                        refreshSDL();
                        break;
                    }
                break;

                default:
                    break;
            }
        }

        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    // Renvoie lID du gagnant
    if(plateau_jeu->nbUniteJoueur(0)==0){
        return(1);
    }else if(plateau_jeu->nbUniteJoueur(1)==0){
        return(0);
    }else{
        return -1;
    }}

// Renvoie l'id du joueur gagant
int mainPlaySolo(Plateau* plateau_jeu,Partie* part1){
    // On sort du Menu et on rentre dans le jeu
    //Booléen qui permet la pause du jeu
    bool pause = false;
    // Boléen qui permlet l'ouverture du magasin
    bool magasin = false;

    // Permet de voir l'unité sélectionné
    bool selected = false;
    int selected_x;
    int selected_y;

    int id_joueur=part1->getIdJoueurTour();

    // Set le deuxième joueur dans
    part1->getJoueur(id_joueur+1)->setIsBot(true);

    // Affichage du joueur qui commence
    afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
    affichagePlateau(plateau_jeu);
    affichageTour(id_joueur,plateau_jeu,part1);

    afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
    affichagePlateau(plateau_jeu);
    refreshSDL();

    /* Boucle principale du jeu */
    int loop =1;
    while(loop)
    {
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT)
			{
				loop = 0;
				break;
			}

			if(	e.type == SDL_KEYDOWN )
			{
                if(e.key.keysym.sym == SDLK_RETURN)
                {
                    if(pause == false){
                        menuPause(plateau_jeu);
                        pause = true;
                        break;
                    }else{
                        affichageSauvegarde(part1);
                        loop = 0;
                        break;
                    }
                }else if(e.key.keysym.sym == SDLK_ESCAPE)
                {
                        afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                        affichagePlateau(plateau_jeu);
                        refreshSDL();
                        pause=false;
                        break;
                }
			}

            /* Quelques exemples de traitement d'evenements : */
            switch(e.type)
            {

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    if(magasin==false){
                        if(selected==false){
                            if(clickUnite(e.button.x, e.button.y, plateau_jeu,id_joueur,part1)==1){
                                selected_x = (int) e.button.x/TAILLE_BLOC;
                                selected_y = (int) e.button.y/TAILLE_BLOC;
                                selected = true;
                                break;
                            }else if(e.button.x> plateau_jeu->getTaille()*TAILLE_BLOC && e.button.x< screen->w && e.button.y> 10*screen->h/12 && e.button.y< 11.5*screen->h/12){
                                part1->finTour();
                                id_joueur=part1->getIdJoueurTour();
                                // Affichage du Tour du bot
                                affichageTour(id_joueur,plateau_jeu,part1);
                                if(part1->getJoueur(id_joueur)->getIsBot()==true){
                                    BotCore* bot = new BotCore(part1->getPlateau(), id_joueur);
                                    bot->jouer(part1);
                                    delete bot;

                                    // Redonne la main au joueur après le bot
                                    part1->finTour();
                                    if(plateau_jeu->nbUniteJoueur(0) ==0 || plateau_jeu->nbUniteJoueur(1)==0){
                                        loop = 0;
                                        break;
                                    }
                                    id_joueur=part1->getIdJoueurTour();
                                    affichageTour(id_joueur,plateau_jeu,part1);
                                }
                                afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                                affichagePlateau(plateau_jeu);
                                refreshSDL();
                                break;
                            }else{
                                break;
                            }
                        }else{
                            if(plateau_jeu->attaquerPossible(selected_x,selected_y,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC) && !plateau_jeu->getAAttaqueUnite(selected_x,selected_y)){
                                attaqueUnite(selected_x,selected_y,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC,plateau_jeu,false,part1);
                                selected = false;

                                // Si il n'y a plus de joueur on sort de la loop
                                if(plateau_jeu->nbUniteJoueur(0) ==0 || plateau_jeu->nbUniteJoueur(1)==0){
                                    loop = 0;
                                    break;
                                }

                                if(!part1->peutJouerUnite(id_joueur)){
                                    part1->finTour();
                                    id_joueur=part1->getIdJoueurTour();
                                    // Affichage du Tour du bot
                                    affichageTour(id_joueur,plateau_jeu,part1);
                                    if(part1->getJoueur(id_joueur)->getIsBot()==true){
                                        BotCore* bot = new BotCore(part1->getPlateau(), id_joueur);
                                        bot->jouer(part1);
                                        delete bot;

                                        // Redonne la main au joueur après le bot
                                        part1->finTour();
                                        if(plateau_jeu->nbUniteJoueur(0) ==0 || plateau_jeu->nbUniteJoueur(1)==0){
                                            loop = 0;
                                            break;
                                        }
                                        id_joueur=part1->getIdJoueurTour();
                                        affichageTour(id_joueur,plateau_jeu,part1);
                                    }
                                }
                                afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                                affichagePlateau(plateau_jeu);
                                refreshSDL();
                                break;
                            }else if(plateau_jeu->deplacementPossible(selected_x,selected_y,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC) && !plateau_jeu->getADeplaceUnite(selected_x,selected_y) && !plateau_jeu->getAAttaqueUnite(selected_x,selected_y))
                            {
                                moveUnite(selected_x,selected_y,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC,plateau_jeu,part1);
                                selected = false;

                                if(plateau_jeu->getCaracteristic(e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC)){
                                    if(plateau_jeu->getAppartenance(e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC)==id_joueur){
                                        affichageMagasin(id_joueur,plateau_jeu,e.button.x/TAILLE_BLOC,e.button.y/TAILLE_BLOC,part1->getJoueur(id_joueur),part1->tabUnitePrix);
                                        magasin = true;
                                        break;
                                    }
                                }

                                if(!part1->peutJouerUnite(id_joueur)){
                                    part1->finTour();
                                    id_joueur=part1->getIdJoueurTour();
                                    // Affichage du Tour du bot
                                    affichageTour(id_joueur,plateau_jeu,part1);
                                    if(part1->getJoueur(id_joueur)->getIsBot()==true){
                                        BotCore* bot = new BotCore(part1->getPlateau(), id_joueur);
                                        bot->jouer(part1);
                                        delete bot;

                                        // Redonne la main au joueur après le bot
                                        part1->finTour();
                                        if(plateau_jeu->nbUniteJoueur(0) ==0 || plateau_jeu->nbUniteJoueur(1)==0){
                                            loop = 0;
                                            break;
                                        }
                                        id_joueur=part1->getIdJoueurTour();
                                        affichageTour(id_joueur,plateau_jeu,part1);
                                    }
                                }

                                afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                                affichagePlateau(plateau_jeu);
                                refreshSDL();
                                break;
                            }else{
                                afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                                affichagePlateau(plateau_jeu);
                                refreshSDL();
                                selected = false;
                                break;
                            }
                        }
                    }else{
                        acheterUnite(plateau_jeu,id_joueur,e.button.x,e.button.y,part1->getJoueur(id_joueur),part1->tabUnitePrix);
                        afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                        affichagePlateau(plateau_jeu);
                        refreshSDL();

                        // On ferme le magasin
                        magasin = false;

                        if(!part1->peutJouerUnite(id_joueur)){
                            part1->finTour();
                            id_joueur=part1->getIdJoueurTour();
                            // Affichage du Tour du bot
                            affichageTour(id_joueur,plateau_jeu,part1);
                            if(part1->getJoueur(id_joueur)->getIsBot()==true){
                                BotCore* bot = new BotCore(part1->getPlateau(), id_joueur);
                                bot->jouer(part1);
                                delete bot;

                                // Redonne la main au joueur après le bot
                                part1->finTour();
                                if(plateau_jeu->nbUniteJoueur(0) ==0 || plateau_jeu->nbUniteJoueur(1)==0){
                                        loop = 0;
                                        break;
                                }
                                id_joueur=part1->getIdJoueurTour();
                                affichageTour(id_joueur,plateau_jeu,part1);
                            }
                        }
                        afficheOnglet(id_joueur, plateau_jeu,selected, part1,selected_x,selected_y);
                        affichagePlateau(plateau_jeu);
                        refreshSDL();
                        break;
                    }
                break;

                default:
                    break;
            }
        }

        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    // Renvoie lID du gagnant
    if(plateau_jeu->nbUniteJoueur(0)==0){
        return(1);
    }else if(plateau_jeu->nbUniteJoueur(1)==0){
        return(0);
    }else{
        return -1;
    }}

void sauvegarde(Partie* partie,std::string cheminFichierTxt){
    partie->sauvgarderPartie(cheminFichierTxt);
}

void affichageSauvegarde(Partie* partie){
    Plateau* demo = new Plateau("data/plateau/Domination.txt");
    TAILLE_BLOC = screen->h/(demo->getTaille()-2);
    afficherUnite(screen,demo);

    glColor4f(255,255,255,1);
    // Affichage texte
    SDL_Color couleur = {250,250,250};
    TTF_Font* fontTitre = TTF_OpenFont("FORCEDSQUARE.ttf", 80);
    if (!fontTitre)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }

    SDL_Rect pos;
    pos.x = 350;
    pos.y = 60;
    SDL_GL_RenderText("SAUVEGARDE", fontTitre, couleur,pos);

    TTF_Font* font = TTF_OpenFont("FORCEDSQUARE.ttf", 40);
    if (!fontTitre)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }

    pos.x = 300;
    pos.y = 420;
    SDL_GL_RenderText("Partie sauvegardee sous le nom : ", font, couleur,pos);

    pos.x = 515;
    pos.y = 470;

    if(nbLigneFichier("listePartieEnregistree.txt")==0){
        sauvegarde(partie,"maPartie1");
        SDL_GL_RenderText("maPartie1 ", font, couleur,pos);
    }else if(nbLigneFichier("listePartieEnregistree.txt")==1){
        sauvegarde(partie,"maPartie2");
        SDL_GL_RenderText("maPartie2 ", font, couleur,pos);
    }else if(nbLigneFichier("listePartieEnregistree.txt")==2){
        sauvegarde(partie,"maPartie3");
        SDL_GL_RenderText("maPartie3", font, couleur,pos);
    }else if(nbLigneFichier("listePartieEnregistree.txt")==3){
        sauvegarde(partie,"maPartie4");
        SDL_GL_RenderText("maPartie4", font, couleur,pos);
    }else{
        sauvegarde(partie, "maPartie");
        SDL_GL_RenderText("maPartie2", font, couleur,pos);
    }

    refreshSDL();
    SDL_Delay(4000);
}
