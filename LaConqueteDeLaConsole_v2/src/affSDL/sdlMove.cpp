#include "sdlMove.h"

int position_Aleatoire_x;
int position_Aleatoire_y;

void affichageAccesDeplacement(int x, int y,Plateau* plateau_jeu, int id_joueur){
    if(id_joueur == 0){
         //Parcours le plateau pour trouver des unités
        for(int i=0;i<plateau_jeu->getTaille(); i++)
        {
            for (int j=0; j<plateau_jeu->getTaille(); j++)
                    {
                       if(plateau_jeu->deplacementPossible(x,y,i,j)){
                            quads_texture(texture_CanBeSelected_R,screen,i,j,0);
                       }
                    }
        }
    }else{
        //Parcours le plateau pour trouver des unités
        for(int i=0;i<plateau_jeu->getTaille(); i++)
        {
            for (int j=0; j<plateau_jeu->getTaille(); j++)
                    {
                       if(plateau_jeu->deplacementPossible(x,y,i,j)){
                            quads_texture(texture_CanBeSelected_V,screen,i,j,0);
                       }
                    }
        }
    }
}

void affichageAccesAttaque(int x, int y,Plateau* plateau_jeu,int id_joueur){
    if(id_joueur == 0){
         //Parcours le plateau pour trouver des unités
        for(int i=0;i<plateau_jeu->getTaille(); i++)
        {
            for (int j=0; j<plateau_jeu->getTaille(); j++)
                    {
                       if(plateau_jeu->attaquerPossible(x,y,i,j)){
                            quads_texture(texture_CanBeAttacked_R,screen,i,j,0);
                       }
                    }
        }
    }else{
         //Parcours le plateau pour trouver des unités
        for(int i=0;i<plateau_jeu->getTaille(); i++)
        {
            for (int j=0; j<plateau_jeu->getTaille(); j++)
                    {
                       if(plateau_jeu->attaquerPossible(x,y,i,j)){
                            quads_texture(texture_CanBeAttacked_V,screen,i,j,0);
                       }
                    }
        }
    }
}

bool clickUnite(int x, int y,Plateau* plateau_jeu, int id_joueur,Partie* partie){
    // Récupère les coordonées du quads en question
    x = (int) x/TAILLE_BLOC;
    y = (int) y/TAILLE_BLOC;

    if(plateau_jeu->uniteExiste(x,y))
    {
        if(plateau_jeu->getIdJoueurUnite(x,y) == id_joueur)
        {
            if(id_joueur == 0)
            {
                // Si l'unité est au joueur, on affiche l'unité selectionné ainsi que les déplacements et attaque possible
                affichagePlateau(plateau_jeu);
                quads_texture(texture_selection_R,screen,x,y,0);

                // Affichage du déplacement
                if(!plateau_jeu->getADeplaceUnite(x,y)){
                    affichageAccesDeplacement(x, y,plateau_jeu,id_joueur);
                }

                if(!plateau_jeu->getAAttaqueUnite(x,y)){
                    affichageAccesAttaque(x, y,plateau_jeu,id_joueur);
                }

                if(plateau_jeu->getADeplaceUnite(x,y) && plateau_jeu->getAAttaqueUnite(x,y)){
                    return false;
                }
                afficheOnglet(id_joueur, plateau_jeu,true, partie,x,y);
                refreshSDL();

                return true;
            }else{
                // Si l'unité est au joueur, on affiche l'unité selectionné ainsi que les déplacements et attaque possible
                affichagePlateau(plateau_jeu);
                quads_texture(texture_selection_V,screen,x,y,0);

                // Affichage du déplacement
                if(!plateau_jeu->getADeplaceUnite(x,y)){
                    affichageAccesDeplacement(x, y,plateau_jeu,id_joueur);
                }

                if(!plateau_jeu->getAAttaqueUnite(x,y)){
                    affichageAccesAttaque(x, y,plateau_jeu,id_joueur);
                }

                if(plateau_jeu->getADeplaceUnite(x,y) && plateau_jeu->getAAttaqueUnite(x,y)){
                    return false;
                }
                afficheOnglet(id_joueur, plateau_jeu,true, partie,x,y);
                refreshSDL();
                return true;
            }
        }
    }
    return false;
}

void moveUnite(int selected_x,int selected_y,int new_x,int new_y,Plateau* plateau_jeu, Partie* partie){
    if(plateau_jeu->deplacementPossible(selected_x,selected_y,new_x,new_y)){
        plateau_jeu->deplacerUnite(selected_x,selected_y,new_x,new_y);
    }
    plateau_jeu->setADeplaceUnite(new_x,new_y,true);
    if(!plateau_jeu->uniteAporte(new_x,new_y)){
        plateau_jeu->setAAttaqueUnite(new_x,new_y,true);
    }
    afficheOnglet(plateau_jeu->getIdJoueurUnite(new_x,new_y), plateau_jeu,false, partie,new_x,new_y);
    affichagePlateau(plateau_jeu);
    refreshSDL();
    SDL_Delay(500);
}

void attaqueUnite(int attaquant_x,int attaquant_y,int attaque_x,int attaque_y,Plateau* plateau_jeu,bool bot, Partie* partie){
    if(plateau_jeu->attaquerPossible(attaquant_x,attaquant_y,attaque_x,attaque_y)){
        affichageAttaque(plateau_jeu,attaquant_x,attaquant_y,attaque_x,attaque_y,bot);
        plateau_jeu->attaquerUnite(attaquant_x,attaquant_y,attaque_x,attaque_y);
    }
    plateau_jeu->setAAttaqueUnite(attaquant_x,attaquant_y,true);
    plateau_jeu->setADeplaceUnite(attaquant_x,attaquant_y,true);
    plateau_jeu->enleverUniteMorte();
    afficheOnglet(plateau_jeu->getIdJoueurUnite(attaquant_x,attaquant_y), plateau_jeu,false, partie,attaquant_x,attaquant_y);
    affichagePlateau(plateau_jeu);
    refreshSDL();
}

void affichageAttaque(Plateau* plateau_jeu, int attaquant_x, int attaquant_y, int attaque_x, int attaque_y, bool bot){
    int startTime=0;
    while(startTime<=45)
    {
        //affichagePlateau(plateau_jeu);
        if(plateau_jeu->getIdJoueurUnite(attaquant_x,attaquant_y)==0){
            if(plateau_jeu->getIdFamilleUnite(attaquant_x,attaquant_y)==0){
                attaque_quads(texture_VR_1_D,true);
            }else if(plateau_jeu->getIdFamilleUnite(attaquant_x,attaquant_y)==1){
                attaque_quads(texture_VR_2_D,true);
            }else{
                attaque_quads(texture_VR_3_D,true);
            }
            affichage_PV(bot);
            if(startTime<20 && startTime%5==0 || startTime>20)
            {
                if(plateau_jeu->getIdFamilleUnite(attaque_x,attaque_y)==0){
                    attaque_quads(texture_VV_1_G,false);
                }else if(plateau_jeu->getIdFamilleUnite(attaque_x,attaque_y)==1){
                    attaque_quads(texture_VV_2_G,false);
                }else{
                    attaque_quads(texture_VV_3_G,false);
                }
            }
            if(startTime<20){
                quad_attaque(texture_Attack,5*screen->w/8 - rand()% (screen->w/4),1.5*screen->w/8 + rand()% (screen->h/4));
            }
            startTime += 1;
        }else{
            if(plateau_jeu->getIdFamilleUnite(attaquant_x,attaquant_y)==0){
                attaque_quads(texture_VV_1_G,false);
            }else if(plateau_jeu->getIdFamilleUnite(attaquant_x,attaquant_y)==1){
                attaque_quads(texture_VV_2_G,false);
            }else{
                attaque_quads(texture_VV_3_G,false);
            }
            affichage_PV(bot);
            if(startTime<20 && startTime%5==0 || startTime>20)
            {
                if(plateau_jeu->getIdFamilleUnite(attaque_x,attaque_y)==0){
                    attaque_quads(texture_VR_1_D,true);
                }else if(plateau_jeu->getIdFamilleUnite(attaque_x,attaque_y)==1){
                    attaque_quads(texture_VR_2_D,true);
                }else{
                    attaque_quads(texture_VR_3_D,true);
                }
            }
            if(startTime<20){
                quad_attaque(texture_Attack,1*screen->w/8 - rand()% (screen->w/4),1.5*screen->w/8 + rand()% (screen->w/4));
            }
            startTime += 1;
        }
        refreshSDL();
        SDL_Delay(50);
    }
}

void positionInnocupe(Plateau* plateau_jeu){
    int x = rand()% plateau_jeu->getTaille();
    int y = rand()% plateau_jeu->getTaille();
    while(plateau_jeu->uniteExiste(x,y)){
        x = rand()% plateau_jeu->getTaille();
        y = rand()% plateau_jeu->getTaille();
    }
    position_Aleatoire_x = x;
    position_Aleatoire_y = y;
}

void acheterUnite(Plateau* plateau_jeu, int id_joueur, int x, int y, Joueur* joueur_current, int tabUnitePrix[]){
    positionInnocupe(plateau_jeu);
    if(x>screen->w/8 && x<screen->w/8+screen->w/4 && y>screen->h/8+50 && y<screen->h/8+screen->h/4+50)
    {
        if(joueur_current->getArgent()>=tabUnitePrix[0]){
            plateau_jeu->ajoutUnite(position_Aleatoire_x,position_Aleatoire_y,id_joueur,0);
            joueur_current->enleverArgent(tabUnitePrix[0]);
        }
    }else if(x>5*screen->w/8 && x<5*screen->w/8+screen->w/4 && y>screen->h/8+50 && y<screen->h/8+screen->h/4+50)
    {
        if(joueur_current->getArgent()>=tabUnitePrix[1]){
            plateau_jeu->ajoutUnite(position_Aleatoire_x,position_Aleatoire_y,id_joueur,1);
            joueur_current->enleverArgent(tabUnitePrix[1]);
        }
    }else if(x>3*screen->w/8 && x<3*screen->w/8+screen->w/4 && y>4*screen->h/8+50 && y<4*screen->h/8+screen->h/4+50)
    {
        if(joueur_current->getArgent()>=tabUnitePrix[2]){
            plateau_jeu->ajoutUnite(position_Aleatoire_x,position_Aleatoire_y,id_joueur,2);
            joueur_current->enleverArgent(tabUnitePrix[2]);
        }
    }
}
