/*
affichageJeu.h
rôle : affiche le plateau de jeu à partir des fichier.txt
*/

#ifndef AFFICHAGEJEU_H
#define AFFICHAGEJEU_H

/*Bibliotheque SDL */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL_ttf.h>

//#include <windows.h>
#include <Plateau.h>
#include <PartieConsole.h>
#include <sdlTools.h>
#include <affichagePlateau.h>
#include <affichageTexte.h>
#include <sdlMove.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// AffichageMenu Debut
void affichageMenuDebut();

/* Affichage pendant le jeu*/
// Affichage du Tour des joueurs
void affichageTour(int id_joueur,Plateau* plateau_jeu, Partie* partie);
void afficheOnglet(int id_joueur,Plateau* plateau_jeu, bool selected,Partie* partie,int x, int y);

// Affichage MenuPause
void menuPause(Plateau* plateau_jeu);
// Affichage Magasin
void affichageMagasin(int id_joueur,Plateau* plateau_jeu,int x, int y,Joueur* joueur_current, int tabUnitePrix[]);

// Affichage MenuFin
void affichageMenuFin(int id_joueur_gagnant);


#endif // AFFICHAGEJEU_H
