#include "affichageJeu.h"

// AffichageMenu Debut
void affichageMenuDebut(){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture_Menu);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f(7*screen->w/8,0,0);

    glTexCoord2f(0,0);
    glVertex3f(screen->w/8,0,0);

    glTexCoord2f(0,1);
    glVertex3f(screen->w/8,screen->h/2,0);

    glTexCoord2f(1,1);
    glVertex3f(7*screen->w/8,screen->h/2,0);

    glEnd();

    glPopMatrix();

    // Affichage des Boutons
    // SOLO
    affichageBoutonMenu(texture_Bouton_Solo,screen,4);
    // MULTI
    affichageBoutonMenu(texture_Bouton_Multi,screen,5);
    // CONTINUER
    affichageBoutonMenu(texture_Bouton_Continuer,screen,6);
    // INFO
    affichageBoutonMenu(texture_Bouton_Info,screen,7);

    refreshSDL();
}

// Affichage pendant le jeu
// Affichage du Tour des joueurs
void affichageTour(int id_joueur,Plateau* plateau_jeu, Partie* partie){
    affichagePlateau(plateau_jeu);
    afficheOnglet(id_joueur, plateau_jeu,false, partie,0,0);
    if(id_joueur == 0){
        // Lie la texture à la matrice
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture_joueur_1);
    }else{
        // Lie la texture à la matrice
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture_joueur_2);
    }

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

        glTexCoord2f(1,0);
        glVertex3f(screen->h,(screen->h)/4,0);

        glTexCoord2f(0,0);
        glVertex3f(0,(screen->h)/4,0);

        glTexCoord2f(0,1);
        glVertex3f(0,3*((screen->h)/4),0);

        glTexCoord2f(1,1);
        glVertex3f(screen->h,3*((screen->h)/4),0);

    glEnd();
    glPopMatrix();

    refreshSDL();
    SDL_Delay(1750);
}

void afficheOnglet(int id_joueur,Plateau* plateau_jeu, bool selected, Partie* partie, int x, int y){
     if(id_joueur == 0){
        // Lie la texture à la matrice
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture_selection_R);
    }else{
        // Lie la texture à la matrice
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture_selection_V);
    }
    glColor4f(255,255,255,1);
    glPushMatrix();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    // Trace un quad
    glBegin(GL_QUADS);
        glTexCoord2f(1,0);
        glVertex3f(screen->w,0,0);
        glTexCoord2f(0,0);
        glVertex3f(plateau_jeu->getTaille()*TAILLE_BLOC,0,0);
        glTexCoord2f(0,1);
        glVertex3f(plateau_jeu->getTaille()*TAILLE_BLOC,screen->h+20,0);
        glTexCoord2f(1,1);
        glVertex3f(screen->w,screen->h+20,0);
    glEnd();
    glPopMatrix();

    // Affiche le joueur
    if(id_joueur == 0){
        // Lie la texture à la matrice
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture_joueur_1);
    }else{
        // Lie la texture à la matrice
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture_joueur_2);
    }
    glColor4f(255,255,255,1);
    glPushMatrix();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    // Trace un quad
    glBegin(GL_QUADS);
        glTexCoord2f(1,0);
        glVertex3f(screen->w,0,0);
        glTexCoord2f(0,0);
        glVertex3f(plateau_jeu->getTaille()*TAILLE_BLOC,0,0);
        glTexCoord2f(0,1);
        glVertex3f(plateau_jeu->getTaille()*TAILLE_BLOC,screen->h/6,0);
        glTexCoord2f(1,1);
        glVertex3f(screen->w,screen->h/6,0);
    glEnd();
    glPopMatrix();

    // Affichage des informations comme le nombre d'unité er l'agent disponible
    texteOnglet(selected, x, y, partie->getJoueur(partie->getIdJoueurTour()), plateau_jeu);

    // Affichage du bouton fin de jeu
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture_bouton_Fin);
    glColor4f(255,255,255,1);
    glPushMatrix();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    // Trace un quad
    glBegin(GL_QUADS);
        glTexCoord2f(1,0);
        glVertex3f(screen->w,10*screen->h/12,0);
        glTexCoord2f(0,0);
        glVertex3f(plateau_jeu->getTaille()*TAILLE_BLOC,10*screen->h/12,0);
        glTexCoord2f(0,1);
        glVertex3f(plateau_jeu->getTaille()*TAILLE_BLOC,11.5*screen->h/12,0);
        glTexCoord2f(1,1);
        glVertex3f(screen->w,11.5*screen->h/12,0);
    glEnd();
    glPopMatrix();

    SDL_Color couleur = {250,250,250};
    TTF_Font* font = TTF_OpenFont("FORCEDSQUARE.ttf", 30);
    if (!font)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }
    SDL_Rect pos;
    pos.x = plateau_jeu->getTaille()*TAILLE_BLOC + 47.5;
    pos.y = 10.62*screen->h/12;
    SDL_GL_RenderText(" FIN DE TOUR ", font, couleur,pos);
}

// Affichage Pause
void menuPause(Plateau* plateau_jeu){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture_pause);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f(7*screen->w/8,0,0);

    glTexCoord2f(0,0);
    glVertex3f(screen->w/8,0,0);

    glTexCoord2f(0,1);
    glVertex3f(screen->w/8,screen->h,0);

    glTexCoord2f(1,1);
    glVertex3f(7*screen->w/8,screen->h,0);

    glEnd();
    glPopMatrix();

    refreshSDL();
}

void affichageMagasin(int id_joueur,Plateau* plateau_jeu, int x, int y, Joueur* joueur_current,int tabUnitePrix[]){
    affichageTexteMagasin(joueur_current,tabUnitePrix);
    logo_magasin();
    if(plateau_jeu->getAppartenance(x,y)==0 || (plateau_jeu->getAppartenance(x,y)==-1 && id_joueur == 0)){
        quads_magasin(texture_VR_1,screen, 1, 1);
        quads_magasin(texture_VR_2,screen, 5, 1);
        quads_magasin(texture_VR_3,screen, 3, 4);
    }else if(plateau_jeu->getAppartenance(x,y)==1 || (plateau_jeu->getAppartenance(x,y)==-1 && id_joueur == 1)){
        quads_magasin(texture_VV_1,screen, 1, 1);
        quads_magasin(texture_VV_2,screen, 5, 1);
        quads_magasin(texture_VV_3,screen, 3, 4);
    }

    // Barre s'il ne peut pas achter l'unité
    if(joueur_current->getArgent()<tabUnitePrix[0])
    {
        quads_magasin(texture_croix,screen, 3, 4);
        quads_magasin(texture_croix,screen, 5, 1);
        quads_magasin(texture_croix,screen, 1, 1);
    }
    else if(joueur_current->getArgent()<tabUnitePrix[1])
    {
        quads_magasin(texture_croix,screen, 3, 4);
        quads_magasin(texture_croix,screen, 5, 1);
    }else if(joueur_current->getArgent()<tabUnitePrix[2])
    {
        quads_magasin(texture_croix,screen, 3, 4);
    }

    refreshSDL();
}

// Affichage MenuFin
void affichageMenuFin(int id_joueur_gagnant){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture_Bravo);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f(3*screen->w/4,0,0);

    glTexCoord2f(0,0);
    glVertex3f(screen->w/4,0,0);

    glTexCoord2f(0,1);
    glVertex3f(screen->w/4,(screen->h - (screen->w/2))/2,0);

    glTexCoord2f(1,1);
    glVertex3f(3*screen->w/4,(screen->h - (screen->w/2))/2,0);

    glEnd();
    glPopMatrix();

    // AFFICHAGE DU GAGNANT
    if(id_joueur_gagnant == 0){
        // Lie la texture à la matrice
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture_joueur_1);
    }else{
        // Lie la texture à la matrice
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture_joueur_2);
    }


    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

        glTexCoord2f(1,0);
        glVertex3f(screen->w,(screen->h - (screen->w/2))/2,0);

        glTexCoord2f(0,0);
        glVertex3f(0,(screen->h - (screen->w/2))/2,0);

        glTexCoord2f(0,1);
        glVertex3f(0,screen->h - (screen->h - (screen->w/2))/2,0);

        glTexCoord2f(1,1);
        glVertex3f(screen->w,screen->h - (screen->h - (screen->w/2))/2,0);

    glEnd();
    glPopMatrix();

    // AFFICHAGE BOUTON
    affichageBoutonFin(texture_Bouton_Rejouer,screen,0);
    affichageBoutonFin(texture_Bouton_Quitter,screen,1);

    refreshSDL();
}
