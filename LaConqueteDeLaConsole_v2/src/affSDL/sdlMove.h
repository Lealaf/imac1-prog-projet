/*
affichagePlateau.h
rôle : affiche le plateau de jeu à partir des fichier.txt
*/

#ifndef SDLMOVE_H
#define SDLMOVE_H

/*Bibliotheque SDL */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>

//#include <windows.h>
#include <Partie.h>
#include <Plateau.h>
#include <sdlTools.h>
#include <affichagePlateau.h>
#include <affichageJeu.h>
#include <affichageTexte.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

extern int position_Aleatoire_x;
extern int position_Aleatoire_y;

// Selection
bool clickUnite(int x, int y,Plateau* plateau_jeu,int id_joueur, Partie* partie);

// Accès
void affichageAccesDeplacement(int x, int y,Plateau* plateau_jeu, int id_joueur);
void affichageAccesAttaque(int x, int y,Plateau* plateau_jeu, int id_joueur);

// Attaque
void moveUnite(int selected_x,int selected_y,int new_x,int new_y,Plateau* plateau_jeu, Partie* partie);
void attaqueUnite(int attaquant_x,int attaquant_y,int attaque_x,int attaque_y,Plateau* plateau_jeu, bool bot, Partie* partie);
// Affichage Attaque
void affichageAttaque(Plateau* plateau_jeu, int attaquant_x, int attaquant_y, int attaque_x, int attaque_y, bool bot);

//Achat
void positionInnocupe(Plateau* plateau_jeu);
void acheterUnite(Plateau* plateau_jeu,int id_joueur, int x, int y,Joueur* joueur_current, int tabUnitePrix[]);

bool test();

#endif // SDLMOVE_H
