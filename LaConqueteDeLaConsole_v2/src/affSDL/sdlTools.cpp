#include "sdlTools.h"
/* Déclaration de variable */
SDL_Surface* screen;
int WINDOW_WIDTH;
int WINDOW_HEIGHT;
int TAILLE_BLOC;
GLuint texture_Magasin;

// Definition de la police
SDL_Color couleurPolice = {255,255,255};
SDL_Color couleurJoueur1 = {152,24,36};
SDL_Color couleurJoueur2 = {115,58,199};

/* Initialisation de la SDL*/
void initialisationSDL(const char* windowTitle){

    /* Initialisation de la SDL */
    if(-1 == SDL_Init(SDL_INIT_VIDEO))
    {
        fprintf(
            stderr,
            "Impossible d'initialiser la SDL. Fin du programme.\n");
        exit(EXIT_FAILURE);
    }

    WINDOW_HEIGHT = 820;
    WINDOW_WIDTH = 1120;

    // Crée une SDL_Surface adapté au plateau
    screen = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 16,SDL_HWSURFACE|SDL_DOUBLEBUF);
    reshape(&screen, WINDOW_WIDTH, WINDOW_HEIGHT);

    /* Initialisation du titre de la fenetre */
    SDL_WM_SetCaption(windowTitle, NULL);

    /* Initialisation de SDL_ttf */
    TTF_Init();
    if(TTF_Init()==-1)
    {
        fprintf(
            stderr,
            "Impossible d'initialiser la SDL_ttf. Fin du programme.\n");
        exit(EXIT_FAILURE);
    }
}

void reshape(SDL_Surface** surface, unsigned int width, unsigned int height)
{
    SDL_Surface* surface_temp = SDL_SetVideoMode(
        width, height, BIT_PER_PIXEL,
        SDL_OPENGL | SDL_GL_DOUBLEBUFFER | SDL_RESIZABLE);
    if(NULL == surface_temp)
    {
        fprintf(
            stderr,
            "Erreur lors du redimensionnement de la fenetre.\n");
        exit(EXIT_FAILURE);
    }
    *surface = surface_temp;

    aspectRatio = width / (float) height;

    glViewport(0, 0, (*surface)->w, (*surface)->h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if( aspectRatio > 1)
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2. * aspectRatio, GL_VIEW_SIZE / 2. * aspectRatio,
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.);
    }
    else
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
        -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
    }
}

void refreshSDL(){
    SDL_GL_SwapBuffers();
    glClearColor(0,0,0,1);
    glClear(GL_COLOR_BUFFER_BIT);
}

void chargerTexture(const char* filename,GLuint texture)
{
    //Charge les images des unitées
    SDL_Surface* Image = IMG_Load(filename);

    // Verifie la bonne importations des images
    if (!Image)
    {
        printf("Unable to load img: %s\n", SDL_GetError());
        SDL_Quit();
    }

    // Pour savoir si on est en RGB ou RGBA
    int mode;
    if(Image->format->BytesPerPixel == 4 ){
        mode = GL_RGBA;
    }else{
        mode = GL_RGB;
    }

    /* Allouer la memoire texture*/
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    /* Copie sur la carte graphique */
    glTexImage2D(GL_TEXTURE_2D,0,mode,Image->w,Image->h,0,mode,GL_UNSIGNED_BYTE,Image->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
}

/* Fonction de Dessin */
void debutDessin(){
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,WINDOW_WIDTH,WINDOW_HEIGHT,0,-5,5);
}

void affichageBoutonMenu(GLuint textures,SDL_Surface* screen, int j){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,textures);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f(2*screen->w/3,(j*screen->h)/8 - 20,0);

    glTexCoord2f(0,0);
    glVertex3f(screen->w/3,(j*screen->h)/8 - 20,0);

    glTexCoord2f(0,1);
    glVertex3f(screen->w/3,((j+1)*screen->h)/8 -20,0);

    glTexCoord2f(1,1);
    glVertex3f(2*screen->w/3,((j+1)*screen->h)/8 -20,0);

    glEnd();

    glPopMatrix();
}

void quads_texture(GLuint textures,SDL_Surface* screen,int i, int j,int z){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,textures);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f(TAILLE_BLOC*(i+1),TAILLE_BLOC*j,z);

    glTexCoord2f(0,0);
    glVertex3f(TAILLE_BLOC*i,TAILLE_BLOC*j,z);

    glTexCoord2f(0,1);
    glVertex3f(TAILLE_BLOC*i,TAILLE_BLOC*(j+1),z);

    glTexCoord2f(1,1);
    glVertex3f(TAILLE_BLOC*(i+1),TAILLE_BLOC*(j+1),z);

    glEnd();

    glPopMatrix();
}

void attaque_quads(GLuint textures, bool left){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,textures);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);
    if(left){
        glTexCoord2f(1,0);
        glVertex3f((screen->w)/2,(screen->h - (screen->w/2))/2,0);

        glTexCoord2f(0,0);
        glVertex3f(0,(screen->h - (screen->w/2))/2,0);

        glTexCoord2f(0,1);
        glVertex3f(0,screen->h - (screen->h - (screen->w/2))/2,0);

        glTexCoord2f(1,1);
        glVertex3f(screen->w/2,screen->h - (screen->h - (screen->w/2))/2,0);
    }else{
        glTexCoord2f(1,0);
        glVertex3f((screen->w),(screen->h - (screen->w/2))/2,0);

        glTexCoord2f(0,0);
        glVertex3f((screen->w)/2,(screen->h - (screen->w/2))/2,0);

        glTexCoord2f(0,1);
        glVertex3f((screen->w)/2,screen->h - (screen->h - (screen->w/2))/2,0);

        glTexCoord2f(1,1);
        glVertex3f(screen->w,screen->h - (screen->h - (screen->w/2))/2,0);
    }
    glEnd();
    glPopMatrix();
}



void quad_attaque(GLuint texture, int i, int j){
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture);

    glColor4f(255,255,255,0.7);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f(i + screen->w/2,j,0);

    glTexCoord2f(0,0);
    glVertex3f(i,j,0);

    glTexCoord2f(0,1);
    glVertex3f(i,j + screen->h/2,0);

    glTexCoord2f(1,1);
    glVertex3f(i + screen->w/2,j + screen->h/2,0);

    glEnd();

    glPopMatrix();

}

void quads_magasin(GLuint textures,SDL_Surface* screen,int i, int j){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,textures);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f((i*screen->w/8) + screen->w/4,j*screen->h/8 + 50,0);

    glTexCoord2f(0,0);
    glVertex3f(i*screen->w/8,j*screen->h/8 + 50,0);

    glTexCoord2f(0,1);
    glVertex3f(i*screen->w/8,j*screen->h/8 + screen->w/4 + 50,0);

    glTexCoord2f(1,1);
    glVertex3f((i*screen->w/8) + screen->w/4,j*screen->h/8 + screen->w/4 + 50,0);

    glEnd();

    glPopMatrix();
}

void logo_magasin(){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texture_Magasin);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f(screen->w,5*screen->h/6,0);

    glTexCoord2f(0,0);
    glVertex3f(screen->w - screen->h/6,5*screen->h/6,0);

    glTexCoord2f(0,1);
    glVertex3f(screen->w - screen->h/6,screen->h,0);

    glTexCoord2f(1,1);
    glVertex3f(screen->w,screen->h,0);

    glEnd();

    glPopMatrix();

}

void affichageBoutonFin(GLuint textures,SDL_Surface* screen, int i){
    // Lie la texture à la matrice
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,textures);

    glColor4f(255,255,255,1);

    glPushMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // Trace un quad
    glBegin(GL_QUADS);

    glTexCoord2f(1,0);
    glVertex3f((i+1)*(screen->w/2),(13.5*screen->h)/16,0);

    glTexCoord2f(0,0);
    glVertex3f((i)*(screen->w/2),(13.5*screen->h)/16,0);

    glTexCoord2f(0,1);
    glVertex3f((i)*(screen->w/2),(16*screen->h)/16,0);

    glTexCoord2f(1,1);
    glVertex3f((i+1)*(screen->w/2),(16*screen->h)/16,0);

    glEnd();

    glPopMatrix();
}

void finDessin(){
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}


/* Fin SDL */
void libererTexture(GLuint textures)
{
    glDeleteTextures(1,&textures);
}

void libererImg(SDL_Surface* surface){
  SDL_FreeSurface(surface);
}

void libereEspace(GLuint textures, SDL_Surface* surface)
{
    libererTexture(textures);
    libererImg(surface);
}

void disabledGL(){
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

void exitSDL(){
    TTF_Quit();
    SDL_Quit();
}

void chargerPolice(int taille, TTF_Font* police){
    police = TTF_OpenFont("./Square.ttf",taille);
    TTF_GetError();
    if(!police){
        printf("OMG, an error : %s", TTF_GetError());
    }
}

void afficherTexte(const char* texte, SDL_Color couleur,SDL_Rect position){

}
