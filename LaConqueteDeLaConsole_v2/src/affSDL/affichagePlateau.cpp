#include "affichagePlateau.h"

// Texture Menu et Pause
GLuint texture_pause;

/* Definition de variable */
GLuint texture_fond_1;
GLuint texture_fond_2;

// Texture Can be Selected
GLuint texture_CanBeSelected_R;
GLuint texture_CanBeSelected_V;

// Texture Can be Selected
GLuint texture_CanBeAttacked_R;
GLuint texture_CanBeAttacked_V;

// Texture attack
GLuint texture_Attack;

// Textures Selection
GLuint texture_selection_V;
GLuint texture_selection_R;

// Texture Joueur
GLuint texture_joueur_1;
GLuint texture_joueur_2;

// Texture croix
GLuint texture_croix;

// Textures d'ordi
GLuint ordi_noir;
GLuint ordi_rouge;
GLuint ordi_violet;
GLuint obstacle;

// Texture de face
GLuint texture_VR_1;
GLuint texture_VR_2;
GLuint texture_VR_3;
GLuint texture_VV_1;
GLuint texture_VV_2;
GLuint texture_VV_3;

// Textures des Virus de dos
GLuint texture_VV_1_D;
GLuint texture_VV_2_D;
GLuint texture_VV_3_D;

// Textures de profil
GLuint texture_VR_1_D;
GLuint texture_VR_2_D;
GLuint texture_VR_3_D;
GLuint texture_VV_1_G;
GLuint texture_VV_2_G;
GLuint texture_VV_3_G;

// Magasin et Menu
GLuint texture_Menu;
GLuint texture_Bravo;

// Bouton
GLuint texture_Bouton_Solo;
GLuint texture_Bouton_Multi;
GLuint texture_Bouton_Info;
GLuint texture_Bouton_Continuer;
GLuint texture_Bouton_Quitter;
GLuint texture_Bouton_Rejouer;
GLuint texture_bouton_Fin;
// Texture
void activeTexture(){
    glGenTextures(1,&texture_pause);
    glGenTextures(1, &texture_Menu);
    chargerTexture("img/affichage/Pause.png",texture_pause);
    chargerTexture("img/affichage/Menu.png",texture_Menu);

    glGenTextures(1,&texture_selection_R);
    glGenTextures(1,&texture_selection_V);
    chargerTexture("img/plateau/Selection_R.png",texture_selection_R);
    chargerTexture("img/plateau/Selection_V.png",texture_selection_V);

    glGenTextures(1,&texture_CanBeSelected_R);
    glGenTextures(1,&texture_CanBeSelected_V);
    chargerTexture("img/plateau/CanBeSelected_R.png",texture_CanBeSelected_R);
    chargerTexture("img/plateau/CanBeSelected_V.png",texture_CanBeSelected_V);

    glGenTextures(1,&texture_CanBeAttacked_R);
    glGenTextures(1,&texture_CanBeAttacked_V);
    chargerTexture("img/plateau/CanBeAttacked_R.png",texture_CanBeAttacked_R);
    chargerTexture("img/plateau/CanBeAttacked_V.png",texture_CanBeAttacked_V);

    glGenTextures(1,&texture_Attack);
    chargerTexture("img/affichage/Attack.png",texture_Attack);

    glGenTextures(1,&texture_fond_1);
    glGenTextures(1,&texture_fond_2);
    chargerTexture("img/plateau/Fond_0.png",texture_fond_1);
    chargerTexture("img/plateau/Fond_1.png",texture_fond_2);

    glGenTextures(1,&texture_joueur_1);
    glGenTextures(1,&texture_joueur_2);
    chargerTexture("img/affichage/Joueur_1.png",texture_joueur_1);
    chargerTexture("img/affichage/Joueur_2.png",texture_joueur_2);

    glGenTextures(1,&ordi_noir);
    glGenTextures(1,&ordi_rouge);
    glGenTextures(1,&ordi_violet);
    glGenTextures(1,&obstacle);
    chargerTexture("img/plateau/Ordi_B.png",ordi_noir);
    chargerTexture("img/plateau/Ordi_R.png",ordi_rouge);
    chargerTexture("img/plateau/Ordi_V.png",ordi_violet);
    chargerTexture("img/plateau/Antivirus.jpg",obstacle);

    glGenTextures(1, &texture_VR_1);
    glGenTextures(1, &texture_VR_2);
    glGenTextures(1, &texture_VR_3);
    glGenTextures(1, &texture_VV_1);
    glGenTextures(1, &texture_VV_2);
    glGenTextures(1, &texture_VV_3);
    glGenTextures(1, &texture_VV_1_D);
    glGenTextures(1, &texture_VV_2_D);
    glGenTextures(1, &texture_VV_3_D);
    chargerTexture("img/unite/V1_R_B.png",texture_VR_1);
    chargerTexture("img/unite/V2_R_B.png",texture_VR_2);
    chargerTexture("img/unite/V3_R_B.png",texture_VR_3);
    chargerTexture("img/unite/V1_V_B.png",texture_VV_1);
    chargerTexture("img/unite/V2_V_B.png",texture_VV_2);
    chargerTexture("img/unite/V3_V_B.png",texture_VV_3);
    chargerTexture("img/unite/V1_V_H.png",texture_VV_1_D);
    chargerTexture("img/unite/V2_V_H.png",texture_VV_2_D);
    chargerTexture("img/unite/V3_V_H.png",texture_VV_3_D);

    glGenTextures(1, &texture_VR_1_D);
    glGenTextures(1, &texture_VR_2_D);
    glGenTextures(1, &texture_VR_3_D);
    glGenTextures(1, &texture_VV_1_G);
    glGenTextures(1, &texture_VV_2_G);
    glGenTextures(1, &texture_VV_3_G);
    chargerTexture("img/unite/V1_R_D.png",texture_VR_1_D);
    chargerTexture("img/unite/V2_R_D.png",texture_VR_2_D);
    chargerTexture("img/unite/V3_R_D.png",texture_VR_3_D);
    chargerTexture("img/unite/V1_V_G.png",texture_VV_1_G);
    chargerTexture("img/unite/V2_V_G.png",texture_VV_2_G);
    chargerTexture("img/unite/V3_V_G.png",texture_VV_3_G);

    glGenTextures(1, &texture_Magasin);
    glGenTextures(1, &texture_croix);
    glGenTextures(1, &texture_Bravo);
    chargerTexture("img/affichage/Magasin.png",texture_Magasin);
    chargerTexture("img/affichage/Croix.png",texture_croix);
    chargerTexture("img/affichage/Bravo.png",texture_Bravo);

    glGenTextures(1, &texture_Bouton_Continuer);
    glGenTextures(1, &texture_Bouton_Quitter);
    glGenTextures(1, &texture_Bouton_Rejouer);
    glGenTextures(1, &texture_Bouton_Solo);
    glGenTextures(1, &texture_Bouton_Multi);
    glGenTextures(1, &texture_Bouton_Info);
    glGenTextures(1, &texture_bouton_Fin);
    chargerTexture("img/affichage/Bouton_Solo.png",texture_Bouton_Solo);
    chargerTexture("img/affichage/Bouton_Quitter.png",texture_Bouton_Quitter);
    chargerTexture("img/affichage/Bouton_Rejouer.png",texture_Bouton_Rejouer);
    chargerTexture("img/affichage/Bouton_Multi.png",texture_Bouton_Multi);
    chargerTexture("img/affichage/Bouton_Continuer.png",texture_Bouton_Continuer);
    chargerTexture("img/affichage/Bouton_Info.png",texture_Bouton_Info);
    chargerTexture("img/affichage/Bouton_Fin.png",texture_bouton_Fin);



}
void desactiveTexture(){
    glDeleteTextures(1, &texture_Menu);
    glDeleteTextures(1,&texture_pause);

    glDeleteTextures(1,&texture_selection_R);
    glDeleteTextures(1,&texture_selection_V);

    glDeleteTextures(1,&texture_CanBeSelected_R);
    glDeleteTextures(1,&texture_CanBeSelected_V);

    glDeleteTextures(1,&texture_CanBeAttacked_R);
    glDeleteTextures(1,&texture_CanBeAttacked_V);

    glDeleteTextures(1,&texture_Attack);

    glDeleteTextures(1,&texture_fond_1);
    glDeleteTextures(1,&texture_fond_2);

    glDeleteTextures(1,&texture_joueur_1);
    glDeleteTextures(1,&texture_joueur_2);

    glDeleteTextures(1,&ordi_noir);
    glDeleteTextures(1,&ordi_rouge);
    glDeleteTextures(1,&ordi_violet);
    glDeleteTextures(1,&obstacle);

    glDeleteTextures(1, &texture_VR_1);
    glDeleteTextures(1, &texture_VR_2);
    glDeleteTextures(1, &texture_VR_3);
    glDeleteTextures(1, &texture_VV_1);
    glDeleteTextures(1, &texture_VV_2);
    glDeleteTextures(1, &texture_VV_3);
    glDeleteTextures(1, &texture_VV_1_D);
    glDeleteTextures(1, &texture_VV_2_D);
    glDeleteTextures(1, &texture_VV_3_D);

    glDeleteTextures(1, &texture_VR_1_D);
    glDeleteTextures(1, &texture_VR_2_D);
    glDeleteTextures(1, &texture_VR_3_D);
    glDeleteTextures(1, &texture_VV_1_G);
    glDeleteTextures(1, &texture_VV_2_G);
    glDeleteTextures(1, &texture_VV_3_G);

    glDeleteTextures(1, &texture_croix);
    glDeleteTextures(1, &texture_Magasin);
    glDeleteTextures(1, &texture_Bravo);

    glDeleteTextures(1, &texture_Bouton_Continuer);
    glDeleteTextures(1, &texture_Bouton_Quitter);
    glDeleteTextures(1, &texture_Bouton_Rejouer);
    glDeleteTextures(1, &texture_Bouton_Solo);
    glDeleteTextures(1, &texture_Bouton_Multi);
    glDeleteTextures(1, &texture_Bouton_Info);
    glDeleteTextures(1, &texture_bouton_Fin);
}

// Affichage Plateau
void afficherFond(SDL_Surface* screen, Plateau* Plateau){
    //Parcours le plateau pour affichr le plateau
    for (int i=0;i<Plateau->getTaille(); i++)
    {
        for (int j=0; j<Plateau->getTaille(); j++)
                {
                    if (Plateau->getCaracteristic(i,j)==1)
                    {
                        if (Plateau->getAppartenance(i,j)== -1)
                        {
                            quads_texture(ordi_noir, screen, i, j,-1);
                        }else if(Plateau->getAppartenance(i,j)== 0)
                        {
                            quads_texture(ordi_rouge, screen, i, j,-1);
                        }else{
                            quads_texture(ordi_violet, screen, i, j,-1);
                        }
                    }else if(Plateau->getCaracteristic(i,j)==2){
                            quads_texture(obstacle, screen, i, j,-1);
                    }else
                    {
                        int random = rand()%2;
                        if(random == 0){
                            quads_texture(texture_fond_1, screen, i, j,-1);
                        }else{
                            quads_texture(texture_fond_2, screen, i, j,-1);
                        }
                    }
                }
    }
}

void afficherUnite(SDL_Surface* screen, Plateau* Plateau)
{
    //Parcours le plateau pour trouver des unit�s
    for (int i=0;i<Plateau->getTaille(); i++)
    {
        for (int j=0; j<Plateau->getTaille(); j++)
                {
                    if (Plateau->uniteExiste(i,j))
                    {
                       if(Plateau->getIdJoueurUnite(i,j)==0)
                        {
                            if(Plateau->getIdFamilleUnite(i,j)==0){
                                quads_texture(texture_VR_1, screen, i, j,0);
                            }else if(Plateau->getIdFamilleUnite(i,j)==1)
                            {
                                quads_texture(texture_VR_2, screen, i, j,0);
                            }else{
                                quads_texture(texture_VR_3, screen, i, j,0);
                            }
                        }else
                        {
                            if(Plateau->getIdFamilleUnite(i,j)==0){
                                quads_texture(texture_VV_1_D, screen, i, j,0);
                            }else if(Plateau->getIdFamilleUnite(i,j)==1)
                            {
                                quads_texture(texture_VV_2_D, screen, i, j,0);
                            }else{
                                quads_texture(texture_VV_3_D, screen, i, j,0);
                            }
                        }
                    }
                }
    }
}


void affichagePlateau(Plateau* plateau_jeu){
    afficherFond(screen,plateau_jeu);
    afficherUnite(screen, plateau_jeu);
}

