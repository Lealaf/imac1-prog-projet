#include "affichageTexte.h"

void SDL_GL_RenderText(const char *text, TTF_Font *font, SDL_Color color,SDL_Rect &rectPos)
{
    SDL_Surface *initial;
    SDL_Surface *intermediary;
    GLuint texture;

    /* Use SDL_TTF to render our text */
    //initial = TTF_RenderText_Blended(font, text, color);
    initial = TTF_RenderText_Solid(font, text, color);

    intermediary = SDL_CreateRGBSurface(0, initial->w, initial->h, 32,0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);

    SDL_BlitSurface(initial, 0, intermediary, 0);

    /* Tell GL about our new texture */
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, initial->w, initial->h, 0, GL_RGBA,
                  GL_UNSIGNED_BYTE, intermediary->pixels );


    /* GL_NEAREST looks horrible, if scaled... */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    /* prepare to render our texture */
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    //glColor3f(1.0f, 1.0f, 1.0f);
    //glColor3f(0.0f, 0.0f, 0.0f);// couleur de fond du texte

    /* Draw a quad at location */
    glBegin(GL_QUADS);
    /* Recall that the origin is in the lower-left corner
       That is why the TexCoords specify different corners
       than the Vertex coors seem to. */

    glTexCoord2f(0.0, 0.0);
      glVertex2f(rectPos.x    , rectPos.y);
    glTexCoord2f(1.0, 0.0);
      glVertex2f(rectPos.x + initial->w, rectPos.y);
    glTexCoord2f(1.0, 1.0);
      glVertex2f(rectPos.x + initial->w, rectPos.y + initial->h);
    glTexCoord2f(0.0, 1.0);
      glVertex2f(rectPos.x    , rectPos.y + initial->h);
    glEnd();

    /* Bad things happen if we delete the texture before it finishes */
    glFinish();

    /* return the deltas in the unused w,h part of the rect */
    rectPos.w = initial->w;
    rectPos.h = initial->h;

    // Nettoyage
    SDL_FreeSurface(initial);
    SDL_FreeSurface(intermediary);
    glDeleteTextures(1, &texture);

}

void affichageIntroSDL(){

    SDL_Color couleur = {250,250,250};
    TTF_Font* fontTitre = TTF_OpenFont("FORCEDSQUARE.ttf", 80);
    if (!fontTitre)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }

    SDL_Rect pos;
    pos.x = 180;
    pos.y = 60;

    SDL_GL_RenderText(" L AIDE DU HACKER", fontTitre, couleur,pos);

    TTF_Font* font = TTF_OpenFont("FORCEDSQUARE.ttf", 25);
    if (!font)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }

    pos.x = 0;
    pos.y = 180;
    SDL_GL_RenderText(" Vous etes a la tete d une armee de virus, emparez-vous du web en ecrasant l'ennemi!", font, couleur,pos);

    pos.x = 0;
    pos.y = 220;
    SDL_GL_RenderText(" Prenez possession des ordinateurs pour acheter de nouveaux virus grace a vos bytecoins !", font, couleur,pos);

    pos.x = 0;
    pos.y = 280;
    SDL_GL_RenderText(" Les categories de virus :", font, couleur,pos);

    pos.x = 0;
    pos.y = 320;
    SDL_GL_RenderText("     - Les virus de type batch, lents et faibles", font, couleur,pos);

    pos.x = 0;
    pos.y = 360;
    SDL_GL_RenderText("     - Les virus de type vers, lies au reseau", font, couleur,pos);

    pos.x = 0;
    pos.y = 400;
    SDL_GL_RenderText("     - Les virus de type bombes logiques, rapides et destructeurs", font, couleur,pos);

    pos.x = 0;
    pos.y = 460;
    SDL_GL_RenderText(" A chaque tour, vous pourrez : ", font, couleur,pos);

    pos.x = 0;
    pos.y = 500;
    SDL_GL_RenderText("     - Acheter des virus en cliquant sur les ordis vous appartenant.", font, couleur,pos);

    pos.x = 0;
    pos.y = 540;
    SDL_GL_RenderText("     - Attaquer ou deplacer puis attaquer, si c'est possible.", font, couleur,pos);

    pos.x = 0;
    pos.y = 600;
    SDL_GL_RenderText(" Pour posseder un ordinateur, placez une unite dessus et au tour suivant il vous appartient.", font, couleur,pos);

    pos.x = 0;
    pos.y = 640;
    SDL_GL_RenderText(" A chaque fin de vos tours, les ordinateurs qui vous appartiennent vous rapporte des bytecoins.", font, couleur,pos);

    pos.x = 930;
    pos.y = 710;
    SDL_GL_RenderText(" OK.", fontTitre, couleur,pos);

    refreshSDL();

    // Tant que le joueur est sur le menu :
    int loop = 1;
    while(loop){
        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();

        /* Boucle traitant les evenements */
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT)
			{
				loop = 0;
				break;
			}

            switch(e.type)
            {

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    if(e.button.x>=660 && e.button.y>=720){
                        loop = 0;
                        break;
                    }
                    break;

                default:
                    break;
            }
        }
        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }
    TTF_CloseFont(fontTitre);
    TTF_CloseFont(font);
}

void texteOnglet(bool selected, int x, int y, Joueur* joueur_current, Plateau* plateau_jeu){
    // Récupère l'argent du joueur et affiche les informations l'écran
    SDL_Color couleur = {250,250,250};
    TTF_Font* font = TTF_OpenFont("FORCEDSQUARE.ttf", 23);
    if (!font)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }
    SDL_Rect pos;
    pos.x = 840;
    pos.y = 3*screen->h/6;
    SDL_GL_RenderText("Informations sur l'unite : ",font, couleur,pos);

    pos.x = 840;
    pos.y = screen->h/6 +20;
    SDL_GL_RenderText("Ressources disponibles :",font, couleur,pos);

    pos.x = 840+20;
    pos.y = screen->h/6 +60;
    char temp[50];
    sprintf(temp, "%d Bytecoins", joueur_current->getArgent());
    SDL_GL_RenderText(temp,font, couleur,pos);



    // Récupère les information sur l'unité selectionné si elle existe
    if(selected){
        // PV
        pos.x = 840+20;
        pos.y = 3*screen->h/6 +40;
        sprintf(temp, "Point de Vie : %f", plateau_jeu->getUnite(x,y)->getPtVie());
        SDL_GL_RenderText(temp,font, couleur,pos);

        // Defense
        pos.x = 840+20;
        pos.y = 3*screen->h/6 + 80;
        sprintf(temp, "Defense : %d", plateau_jeu->getUnite(x,y)->getDefence());
        SDL_GL_RenderText(temp,font, couleur,pos);

        // Porte
        pos.x = 840+20;
        pos.y = 3*screen->h/6 + 120;
        sprintf(temp, "Porte : %d", plateau_jeu->getUnite(x,y)->getPorte());
        SDL_GL_RenderText(temp,font, couleur,pos);

        // Force
        pos.x = 840+20;
        pos.y = 3*screen->h/6 + 160;
        sprintf(temp, "Force : %f", plateau_jeu->getUnite(x,y)->getForce());
        SDL_GL_RenderText(temp,font, couleur,pos);

        // Mobilité
        pos.x = 840+20;
        pos.y = 3*screen->h/6 + 200;
        sprintf(temp, "Mobilite : %d", plateau_jeu->getUnite(x,y)->getMobilite());
        SDL_GL_RenderText(temp,font, couleur,pos);

    }

}
void affichageTexteMagasin(Joueur* joueur_current,int tabUnitePrix[]){
    // Récupère l'argent du joueur et affiche les informations l'écran
    SDL_Color couleur = {250,250,250};
    TTF_Font* font = TTF_OpenFont("FORCEDSQUARE.ttf", 60);
    if (!font)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }
    SDL_Rect pos;
    pos.x = 40;
    pos.y = 20;
    char temp[50];
    sprintf(temp, " Vous disposez de %d Bytecoins.", joueur_current->getArgent());
    SDL_GL_RenderText(temp, font, couleur,pos);
    pos.x = 40;
    pos.y = 70;
    SDL_GL_RenderText(" Quel virus voulez-vous acheter ?", font, couleur,pos);


    // Affichage du prix
    TTF_Font* fontPrix = TTF_OpenFont("FORCEDSQUARE.ttf", 30);
    if (!font)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }
    pos.x = screen->w/8;
    pos.y = screen->h/8 + screen->w/4 + 60;
    sprintf(temp, " Prix : %d Bytecoins.", tabUnitePrix[0]);
    SDL_GL_RenderText(temp, fontPrix, couleur,pos);

    pos.x = 5*screen->w/8;
    pos.y = screen->h/8 + screen->w/4 + 60;
    sprintf(temp, " Prix : %d Bytecoins.", tabUnitePrix[1]);
    SDL_GL_RenderText(temp, fontPrix, couleur,pos);

    pos.x = 3*screen->w/8;
    pos.y = 4*screen->h/8 + screen->w/4 + 60;
    sprintf(temp, " Prix : %d Bytecoins.", tabUnitePrix[2]);
    SDL_GL_RenderText(temp, fontPrix, couleur,pos);

    TTF_CloseFont(font);
}

void affichage_PV(bool bot)
{
    // Affichage du mot PV
    SDL_Color couleur = {250,250,250};
    TTF_Font* font = TTF_OpenFont("FORCEDSQUARE.ttf", 60);
    if (!font)
    {
        printf("Unable to load bitmap: %s\n", TTF_GetError());
    }

    SDL_Rect pos;
    pos.x = 80;
    pos.y = (screen->h - (screen->h - (screen->w/2))/2) + 40;

    if(bot){
        SDL_GL_RenderText(" Attention ! Un bot vous attaque !", font, couleur,pos);
    }else{
        SDL_GL_RenderText(" Vous attaquez une unite adverse !", font, couleur,pos);
    }
}

