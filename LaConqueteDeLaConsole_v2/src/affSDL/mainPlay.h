/*
mainPlay.h
rôle : affiche le plateau de jeu à partir des fichier.txt
*/

#ifndef MAINPLAY_H
#define MAINPLAY_H

/*Bibliotheque SDL */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>

//#include <windows.h>
#include <Plateau.h>
#include <sdlTools.h>
#include <affichagePlateau.h>
#include <affichageJeu.h>
#include <BotCore.h>
#include <Partie.h>
#include <sdlMove.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <Joueur.h>


// Jeu principal : Renvoie l'id du joueur gagnant
int mainPlayMulti(Plateau* plateau_jeu,Partie* part1);

// Jeu Solo
int mainPlaySolo(Plateau* plateau_jeu,Partie* part1);

// Save
void sauvegarde(Partie* partie,std::string cheminFichierTxt);
void affichageSauvegarde(Partie* partie);

#endif // MAINPLAY_H
