#include "PartieConsole.h"
#include "fonctionAffichageConsole.h"

using namespace std;

PartieConsole::PartieConsole(std::string nomListeTxt, int num)
{
    partieEnCours = new Partie(nomListeTxt,num);
}


PartieConsole::~PartieConsole()
{
    delete partieEnCours;
}

void PartieConsole::lancerPartie()
{
    system("cls");
    while ( partieEnCours->joueurGagnant() == -1 && partieEnCours->getContinuerPartie() ){
        tour();
        partieEnCours->finTour();

    }

    affichageIntro();
    if (partieEnCours->joueurGagnant() != -1)
    {
        cout<< espacement() << "Le joueur "<<partieEnCours->joueurGagnant()<<" a gagne!"<<endl;
        cout<< espacement() << "Bravo!"<<endl;
        entreeContinue();
    }else
    {
        cout<< espacement() << "Vous avez fuit le combat!"<<endl;
    }
}


void PartieConsole::tour()
{
    ATH(partieEnCours->getIdJoueurTour(),partieEnCours->getJoueur( partieEnCours->getIdJoueurTour())->getArgent());
    cout<< espacement() << "C'est au joueur "<<partieEnCours->getIdJoueurTour()<<" pour arreter taper 'q' ou autre lettre pour continuer"<<endl;
    char arreter;
    cin >> arreter;
    if (arreter == 'q')
    {
        partieEnCours->setContinuerPartie(false);
    }else
    {
        bool continuerDeJouer = true;
        int pX;
        int pY;
        while(continuerDeJouer && partieEnCours->joueurGagnant() == -1)
        {
            afficherPlateau(partieEnCours->getPlateau());
            ATH(partieEnCours->getIdJoueurTour(),partieEnCours->getJoueur( partieEnCours->getIdJoueurTour())->getArgent());

            choixCase(pX, pY);
            actionCase(pX, pY);

            afficherPlateau(partieEnCours->getPlateau());
            ATH(partieEnCours->getIdJoueurTour(),partieEnCours->getJoueur( partieEnCours->getIdJoueurTour())->getArgent());

            if (partieEnCours->joueurGagnant() == -1)
            {
                affichageTexte("Avez-vous terminer de jouer ? (o/n)");
                char reponse;
                cin >> reponse;
                if (reponse == 'o')
                {
                    afficherPlateau(partieEnCours->getPlateau());
                    continuerDeJouer=false;
                }
            }
        }
    }
}


void PartieConsole::choixCase(int& pX, int& pY)
{
    char caracterePX;
    char caracterePY;
    do
    {
        afficherPlateau(partieEnCours->getPlateau());
        ATH(partieEnCours->getIdJoueurTour(),partieEnCours->getJoueur( partieEnCours->getIdJoueurTour())->getArgent());
        affichageTexte("Choisissez une case, X puis Y.");
        cin >> caracterePX >> caracterePY;
        pX =charToInt(caracterePX);
        pY =charToInt(caracterePY);
    }
    while (pX<0 || pX >= partieEnCours->getPlateau()->getTaille()|| pY<0 || pY >= partieEnCours->getPlateau()->getTaille());
}

void PartieConsole::actionCase(int pX, int pY)
{
     if (partieEnCours->getPlateau()->uniteExiste(pX, pY))
    {
        if (partieEnCours->getPlateau()->getIdJoueurUnite(pX,pY)== partieEnCours->getIdJoueurTour())
        {
            affichageTexte("Ceci est votre unite");
            if (partieEnCours->getPlateau()->getADeplaceUnite(pX,pY))
            {
                affichageTexte("Vous avez deja joue cette Bug.");
                afficherInfoUnite(partieEnCours->getPlateau()->getUnite(pX,pY));
                entreeContinue();
            }else
            {
                actionUnite(pX,pY);
            }
        } else
        {
            afficherPlateau(partieEnCours->getPlateau());
            ATH(partieEnCours->getIdJoueurTour(),partieEnCours->getJoueur( partieEnCours->getIdJoueurTour())->getArgent());
            affichageTexte("Cette Bug appartient a l'ennemi.");
            afficherInfoUnite(partieEnCours->getPlateau()->getUnite(pX,pY));
            entreeContinue();
        }

    }else if (partieEnCours->getPlateau()->getCaracteristic(pX, pY)== 1)
    {
        if (partieEnCours->getPlateau()->getAppartenance(pX, pY)== partieEnCours->getIdJoueurTour())
        {
            affichageTexte("Ceci est votre ordinateur");
            if (actionAchat(pX, pY))
            {
                affichageTexte("Bug achetee.");
            }
        } else if (partieEnCours->getPlateau()->getAppartenance(pX, pY)!= -1)
        {
            affichageTexte("Cette ordinateur appartient a l'ennemi.");
        } else
        {
            affichageTexte("Cette ordinateur n'a pas de proprietaire.");
        }
        entreeContinue();
    }else
    {
        affichageTexte("Choisissez une autre case.");
        entreeContinue();
    }
}

bool PartieConsole::actionAchat(int pX, int pY)
{
    char idUniteTxt;
    int idUnite;
    do
    {
        affichageTexte("Quelle bug souhaitez-vous acheter? (0, 1, 2, 3, q)");
        affichageTexte("   0 : -expected : ;- pour 20");
        affichageTexte("   1 : -not declered in this scope- pour 30");
        affichageTexte("   2 : -conversion type- pour 30");
        affichageTexte("   3 : -segmentation fault- pour 30");
        cin>>idUniteTxt;
        if (idUniteTxt== 'q')
        {
            return false;
        }
        else
        {
            idUnite= charToInt(idUniteTxt);
        }
    }while (!partieEnCours->acheterUnite(pX,pY,idUnite));
    return true;
}

void PartieConsole::actionUnite(int pX, int pY)
{
    assert(partieEnCours->getPlateau()->uniteExiste(pX,pY));
    bool actionCorrect = false;
    do // recommence tant que: !actionCorrect
    {
        afficherPlateau(partieEnCours->getPlateau());
        ATH(partieEnCours->getIdJoueurTour(),partieEnCours->getJoueur( partieEnCours->getIdJoueurTour())->getArgent());
        affichageTexte("Que souhaitez-vous faire avec ce Bug?");
        affichageTexte("d: deplacer");
        affichageTexte("a: attaquer");
        affichageTexte("i: informations");
        affichageTexte("q: quitter");
        char action;
        cin>>action;

        switch (action)
        {
            case 'd': // deplacer
                if (actionDeplacer(pX, pY))
                {
                    actionCorrect=true;
                    partieEnCours->getPlateau()->setADeplaceUnite(pX, pY, true);
                }else
                {
                    actionCorrect=false;
                }
                break;

            case 'a': // attaquer
                if (actionAttaquer(pX, pY))
                {
                    actionCorrect=true;
                    partieEnCours->getPlateau()->setAAttaqueUnite(pX, pY, true);
                }else
                {
                    actionCorrect=false;
                }
                break;

            case 'i': // info
                actionInfoUnite ();
                actionCorrect= false;
                break;

            case 'q':
                actionCorrect= true;
                break;

            default:
                affichageTexte("Commande incorecte.");
        }
    }while (!actionCorrect && partieEnCours->joueurGagnant()==-1);
}

bool PartieConsole::actionDeplacer(int& pX, int& pY)
{
    int newPx;
    int newPy;
    char quitte;
    cout << espacement() << "Deplacer de ("<<pX<<","<<pY<<") vers ?"<<endl;
    cin>>newPx>>newPy;
    while (!partieEnCours->getPlateau()->deplacerUnite(pX, pY, newPx, newPy)) //tant que d�placement n'a pas march�
    {
        affichageTexte("Impossible de deplacer ici. q pour quitter ou autre lettre pour continuer.");
        cin >> quitte;
        if (quitte== 'q')
        {
            return false;
        }
        else
        {
            cout << espacement() << "Deplacer de ("<<pX<<","<<pY<<") vers ?"<<endl;
            cin>>newPx>>newPy;
        }
    }
    pX=newPx;
    pY=newPy;
    return true;
}

bool PartieConsole::actionAttaquer(int pX, int pY)
{
    int pXCible;
    int pYCible;
    char quitte;

    cout << espacement() << "Bug ("<<pX<<","<<pY<<") attaque ?"<<endl;
    cin>>pXCible>>pYCible;

    while (!partieEnCours->getPlateau()->attaquerUnite(pX,pY,pXCible,pYCible))
    {
        affichageTexte("Attaque impossible ! q pour quitter ou autre lettre pour continuer");
        cin >> quitte;
        if (quitte== 'q')
        {
            return false;
        }
        else
        {
            cout << espacement() << "Bug ("<<pX<<","<<pY<<") attaque ?"<<endl;
            cin>>pXCible>>pYCible;
        }
    }
    afficherAttaque(partieEnCours->getPlateau()->getUnite(pX,pY), partieEnCours->getPlateau()->getUnite(pXCible,pYCible));
    partieEnCours->getPlateau()->enleverUniteMorte();
    return true;
}

void PartieConsole::actionInfoUnite()
{
    int pX;
    int pY;
    do
    {
        affichageTexte("Quelle est le Bug dont vous voulez connaitre les secrets ?");
        cin>>pX>>pY;
    }
    while (!partieEnCours->getPlateau()->uniteExiste(pX, pY));   // faut que l'uniter existe

    afficherInfoUnite(partieEnCours->getPlateau()->getUnite(pX,pY));
    entreeContinue();
}

void PartieConsole::finPartieConsole()
{
    if (partieEnCours->joueurGagnant() == -1)
    {
        char enregistrer;
        affichageTexte("Souhaitez-vous sauvegader la partie ? (o/n)");
        cin >> enregistrer;
        if (enregistrer == 'o')
        {
            string nomF;
            string nomFTxt;
            do
            {
                affichageTexte("Nom du fichier ?");
                cin >> nomF;
                nomFTxt = nomF+".txt";

            }
            while (nomCorrect("listePartieEnregistree.txt", nomFTxt));

            partieEnCours->sauvgarderPartie(nomF);
            affichageTexte("Fichier enregistre.");
            entreeContinue();
        }
    }
}
