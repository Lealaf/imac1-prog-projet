#include "fonctionAffichageConsole.h"
using namespace std;

void affichageTexte(string text){

    cout << espacement() << text << endl << endl;
}

string espacement()
{
    return "\t";
}

void ATH(int nomJoueur, int argent){

    cout << endl <<endl
    << espacement() << "o-------------------------------o" << endl
    << espacement() << espacement() << "Joueur " << nomJoueur << " | ram : " << argent << endl
    << espacement() << "o-------------------------------o" << endl
    << endl;
}

void affichageIntro()
{
    system("cls");
    cout << endl << endl
    << espacement() << "o====================================o" << endl
    << espacement() << "|    LA CONQUETE DE LA CONSOLE v1    |" << endl
    << espacement() << "o====================================o" << endl << endl;
}


void afficherAide()
{
    affichageIntro();
    cout
    << espacement() << espacement() << "L'AIDE DE LA CONSOLE" << endl << endl
    << espacement() << "Vous etes a la tete d�une armee de bugs, emparez-vous de la console en ecrasant l'ennemi!" << endl
    << espacement() << "Prenez possession des ordinateurs pour creer de nouveaux bugs grece � votre memoire vive !" << endl << endl
    << espacement() << "Les unites sont repr�sentees par deux nombres separes par un tiret (exemple : 0-1)"<< endl
    << espacement() << "Le premier chiffre correspond � l�ID du joueur"<< endl
    << espacement() << "et le deuxi�me � la cat�gorie de l�unit�."<< endl
    << espacement() << "Les categories de bug :"<< endl
    << espacement() << "   0 : �expected : ;�"<< endl
    << espacement() << "   1 : �not declered in this scope�"<< endl
    << espacement() << "   2 : �conversion type�"<< endl
    << espacement() << "   3 : �segmentation fault�"<< endl<< endl
    << espacement() << "Les ordinateurs sont symbolis�s par ^ suivit de l�id du joueur le possedant. "<< endl
    << espacement() << "A chaque tour, choisissez une case du plateau,  qui vous proposera differentes option"<< endl
    << espacement() << "   case vide: rien"<< endl
    << espacement() << "   maison n'appartenant pas au joueur: rien"<< endl
    << espacement() << "   maison appartenant au joueur: acheter une nouvelle unit�"<< endl
    << espacement() << "   unit� ennemis : affichage des caract�ristique de l�unit�"<< endl
    << espacement() << "   unit� joueur: attaquer, d�placer, information;"<< endl<< endl
    << espacement() << "Pour poss�der un ordinateur, il faut placer une unit� dessus et au tour suivant il vous appartient."<< endl
    << espacement() << "A chaque fin de vos tours, les ordinateurs qui vous appartiennent vous rapporte de la m�moire vive."<< endl
    << espacement() << "--------------------------------------" << endl;

    entreeContinue();
    system("cls");
}

bool entreeContinue()
{
    cout << espacement() << "Pressez entree pour continuer..." << endl;
    cin.sync();
    cin.ignore();
    return true;
}

void afficherPlateau(Plateau* plateau)
{
    system("cls");
    cout<< endl << endl << espacement() << "    |";
    for (int i=0; i<plateau->getTaille(); i++)
    {
        cout << " " << i << " |";
    }
    cout<<endl;
    cout<< espacement() <<  "    _";
    for (int i = 0; i<plateau->getTaille(); i++)
    {
        cout<<"____";
    }
    cout<<endl;
    for (int j=0; j<plateau->getTaille(); j++)
    {
        cout<< espacement() << " "<<j<<"  |";
        for (int i=0; i<plateau->getTaille(); i++)
        {
            if (plateau->uniteExiste(i,j))
            {
                cout<< plateau->getIdJoueurUnite(i,j) <<"-"<<plateau->getIdFamilleUnite(i,j);
            }
            else if (plateau->getCaracteristic(i,j)==1)
            {
                cout<<" ^";
                if (plateau->getAppartenance(i,j)== -1)
                {
                    cout<<" ";
                }else
                {
                    cout<<plateau->getAppartenance(i,j)<<"";
                }
            }else if (plateau->getCaracteristic(i,j)==2)
            {
                cout<<" "<<plateau->getCaracteristic(i,j)<<" ";
            }else
            {
                cout<<"   ";
            }
            cout<<"|";
        }
        cout<<endl;
        cout<< espacement() <<  "    _";
        for (int i=0; i<plateau->getTaille(); i++)
        {
            cout<<"____";
        }
        cout<<endl;
    }
    cout<<endl;

}

void afficherAttaque(Unite* attaquant, Unite* cible)
{
    system("cls");

    cout << endl << endl;
    int ptsCible = cible->getPtVie();
    if( ptsCible < 0 ) ptsCible = 0;

    if ( cible->getPtVie() == 0 ) {
        affichageTexte("Vous avez anneanti l'unite ! Bravo !");
    }
    else {
        affichageTexte("Elle n'est pas encore morte, continuez vos efforts!");
    }
    cout << espacement() << "Votre unite: " << attaquant->getPtVie() << " - Ennemi: " << ptsCible << endl;

    entreeContinue();
}


void afficherInfoUnite(Unite* uniAAfficher)
{
    cout << espacement() << espacement() << "Information sur l'unit�"<< endl;
    cout << espacement() << "Proprietaire : "<<uniAAfficher->getIdJoueur()<< endl;
    cout << espacement() << "Famille : "<<uniAAfficher->getIdFamille()<< endl;
    cout << espacement() << "Points de vie: "<<uniAAfficher->getPtVie()<< endl;
    cout << espacement() << "Force : "<<uniAAfficher->getForce()<< endl;
    cout << espacement() << "Portee d'attaque : "<<uniAAfficher->getPorte()<< endl;
    cout << espacement() << "Deplacement max: "<<uniAAfficher->getMobilite()<< endl;
    cout << endl;
}

void afficherLigneFichier(std::string nomListeTxt)
{
    string const nomLTxt = "./data/"+nomListeTxt;
    ifstream fichier(nomLTxt.c_str());
    assert (fichier.is_open());

    string ligne;
    int nombrePlateau=0;
    while ( std::getline( fichier, ligne ) )
    {
        // afficher la ligne � l'�cran
        cout << espacement() << nombrePlateau<<": "<< ligne << endl;
        nombrePlateau++;
    }
    fichier.close();
    cout << endl;
}
