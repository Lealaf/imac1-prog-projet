#include "fonctionMenuConsole.h"

using namespace std;

bool nouvellePartie( int & numFichier);
bool partieEnregistee(int & numFichier);

bool menuJouerQuitter()
{
    bool actionOK =false;
    while(! actionOK)
    {
        affichageIntro();
        cout << espacement() << "1: Jouer" << endl;
        cout << espacement() << "2: Aide / Controles" << endl;
        cout << espacement() << "3: Quitter" << endl;
        char action;
        cin>> action;
        system("cls");
        switch (action)
        {
        case '1':
            actionOK = true;
            return true;
            break;
        case '2':
            afficherAide();
            break;
        case '3':
            actionOK =true;
            system("cls");
            return false;
            break;
        default: system("cls"); break;
        }
    }
    return false;
}

bool menuNewChargerPartieQuitter(std::string & nomListeTxt, int & num)
{
    bool actionOK= false;
    while(! actionOK)
    {
        system("cls");
        affichageIntro();
        cout
        << espacement() << "Bienvenue dans la console...Que souhaitez-vous faire ?" << endl <<endl
        << espacement() << "1: Nouvelle partie" << endl
        << espacement() << "2: Charger partie" << endl
        << espacement() << "3: Quitter" << endl;
        char action;
        cin>> action;
        switch (action)
        {
        case '1':
            if (nouvellePartie(num))
            {
                nomListeTxt="listeFichier.txt";
                actionOK = true;
                return true;
            }else
            {
                actionOK = false;
            }
            break;
        case '2':
            if (partieEnregistee(num))
            {
                nomListeTxt="listePartieEnregistree.txt";
                actionOK = true;
                return true;
            }else
            {
                actionOK = false;
            }
            break;
        case '3':
            system("cls");
            return false;
            break;
        default: system("cls"); break;
        }
    }
    return false;
}

bool nouvellePartie(int & numFichier)
{
    char action;
    do
    {
        affichageIntro();
        afficherLigneFichier("listeFichier.txt");
        affichageTexte("Choisissez votre plateau, ou 'q' pour retournez au menu");
        cin>> action;
    } while(  charToInt(action) >= nbLigneFichier("listeFichier.txt") && action!='q' );
    if ( action != 'q' )
    {
        numFichier = charToInt(action);
        return true;
    }
    return false;
}

bool partieEnregistee(int & numFichier)
{
    afficherLigneFichier("listePartieEnregistree.txt");
    cout <<"lequel? ou 'q' pour retour"<<endl;
    char action;
    cin>> action;
    if (abs((int)action-48)< nbLigneFichier("listeFichier.txt") )
    {
        numFichier = (int)action-48;
        return true;
    }
    return false;
}
