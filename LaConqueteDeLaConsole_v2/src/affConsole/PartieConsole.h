#ifndef PARTIECONSOLE_H
#define PARTIECONSOLE_H

#include <string>

#include "fonctionFichier.h"
#include "fonctionAffichageConsole.h"
#include "Partie.h"

class PartieConsole
{
    private:
        Partie* partieEnCours;

        void tour();
        void choixCase (int&  pX, int&  pY);
        void actionCase(int pX, int pY);
        bool actionAchat (int pX, int pY);
        void actionUnite(int pX, int pY);
        bool actionDeplacer (int & pX, int & pY);// true quand deplacement reussi
        bool actionAttaquer (int pX, int pY);
        void actionInfoUnite ();
        void actionVille(int pX, int pY);

    public:
        PartieConsole(std::string  nomListeTxt, int num);
        virtual ~PartieConsole();

        void lancerPartie();
        void finPartieConsole();

    protected:


};

#endif // PARTIECONSOLE_H
