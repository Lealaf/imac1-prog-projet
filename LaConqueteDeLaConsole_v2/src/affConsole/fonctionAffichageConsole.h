#ifndef FONCTIONAFFICHAGECONSOLE_H
#define FONCTIONAFFICHAGECONSOLE_H

#include <iostream>
#include "Plateau.h"
#include "Unite.h"

void afficherAide (); //Affiche l'aide du jeu
void afficherPlateau (Plateau* plateau);
void afficherAttaque (Unite* attaquant, Unite* cible);
void afficherInfoUnite (Unite* uniAAfficher);
void afficherLigneFichier (std::string nomListeTxt);

void affichageTexte(std::string text);
void ATH(int nomJoueur, int argent);// affiche les informations du joueur
void affichageIntro();
std::string espacement();
bool entreeContinue(); // Fait une pause jusqu'a qu'on apppuie sur entree

#endif // FONCTIONAFFICHAGECONSOLE_H
